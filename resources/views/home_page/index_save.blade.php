<!DOCTYPE html>
<html lang="zxx">

<!-- Mirrored from storage.googleapis.com/themevessel-products/cmart/car-list-leftsidebar.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Apr 2020 10:20:47 GMT -->
<head>
    <title>228 - Car Import</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/bootstrap-submenu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/magnific-popup.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/leaflet.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/map.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('template/home_page/fonts/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/fonts/flaticon/font/flaticon.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/fonts/linearicons/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/jquery.mCustomScrollbar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/dropzone.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/lightbox.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/jnoty.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/slick.css') }}">

    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/style.css') }}">
    <link rel="stylesheet" type="text/css" id="style_sheet"
          href="{{ asset('template/home_page/css/skins/midnight-blue.css') }}">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{ asset('template/home_page/img/favicon.ico') }}" type="image/x-icon">
{{--    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >--}}

<!-- Google fonts -->
    <link href="{{ asset('template/home_page/fonts/google/googleapis.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/fonts/google/googleapis_raleway.css') }}">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('template/home_page/css/ie10-viewport-bug-workaround.css') }}">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script  src="{{ asset('template/home_page/js/ie8-responsive-file-warning.js') }}"></script><![endif]-->
    <script src="{{ asset('template/home_page/js/ie-emulation-modes-warning.js') }}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script  src="{{ asset('template/home_page/js/html5shiv.min.js') }}"></script>
    <script  src="{{ asset('template/home_page/js/respond.min.js') }}"></script>
    <![endif]-->
</head>
<body>
<div class="page_loader"></div>

<!-- Main header start -->
<header class="main-header sticky-header header-with-top">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand company-logo" href="{{ route('home.page') }}">
                <img src="{{ asset('template/home_page/img/logos/black-logo.png') }}" alt="logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
                    aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fa fa-bars"></span>
            </button>
            <div class="navbar-collapse collapse w-100" id="navbar">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            Acceuil
                        </a>
                    </li>
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            Liste des Cars
                        </a>
                    </li>
                    <li class="nav-item dropdown megamenu-li">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">Pages</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink6" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            Blog
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink7" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            Shop
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#">Contact</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
<!-- Main header end -->

<!-- Sub banner start -->
<div class="sub-banner">
    <div class="container breadcrumb-area">
        <div class="breadcrumb-areas">
            <h1>Bienvenue sur 228 Car</h1>
            <ul class="breadcrumbs">
                <li><h6 style="color: white">Trouvez votre voiture de rêve</h6></li>
            </ul>
        </div>
    </div>
</div>
<!-- Sub Banner end -->

<!-- Featured car start -->
<div class="featured-car content-area">
    <div class="container">
        <div class="row">
            @if($cars_to_select->isNotEmpty())
                <div class="col-lg-4 col-md-12">
                    <div class="sidebar-left">
                        <!-- Advanced search start -->
                        <div class="widget advanced-search2 widget-3">
                            <h3 class="sidebar-title">{{__('Search Cars')}}</h3>

                            <form method="GET" action="{{ route('home.page') }}">
                                <div class="form-group">
                                    <select class="selectpicker search-fields" name="select-brand">
                                        <option value="">{{__('Select Brand')}}</option>
                                        @foreach($brands as $brand)
                                            <option value="{{ $brand }}">{{ $brand }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="selectpicker search-fields" name="select-make">
                                        <option value="">{{__('Select Make')}}</option>
                                        @foreach($makes as $make)
                                            <option value="{{ $make }}">{{ $make }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="selectpicker search-fields" name="select-location">
                                        <option value="">{{__('Select Location')}}</option>
                                        @foreach($locations as $location)
                                            <option value="{{ $location }}">{{ $location }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="selectpicker search-fields" name="select-year">
                                        <option value="">{{__('Select Year')}}</option>
                                        @foreach($years as $year)
                                            <option value="{{ $year }}">{{ $year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="selectpicker search-fields" name="select-type">
                                        <option value="">{{__('Select Type Of Car')}}</option>
                                        @foreach($types as $type)
                                            <option value="{{ $type }}">{{ $type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group mb-0">
                                    <button class="search-button btn">{{__('Search')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @if($cars->isNotEmpty())
                    <div class="col-lg-8 col-md-12">
                        <!-- Option bar start -->
                        <div class="option-bar clearfix">
                            {{--<div class="sorting-options2">
                                <span class="sort">Sort by:</span>
                                <select class="selectpicker search-fields" name="default-order">
                                    <option>Default Order</option>
                                    <option>Price High to Low</option>
                                    <option>Price: Low to High</option>
                                    <option>Newest Properties</option>
                                    <option>Oldest Properties</option>
                                </select>
                            </div>
                            <div class="sorting-options float-right">
                                <a href="car-list-leftsidebar.html" class="change-view-btn active-view-btn"><i
                                        class="fa fa-th-list"></i></a>
                                <a href="car-grid-leftside.html" class="change-view-btn"><i class="fa fa-th-large"></i></a>
                            </div>--}}
                        </div>
                        <!-- Car box 2 start -->
                        <!-- Display Cars -->
                        @foreach($cars as $car)
                            <div class="car-box-2">
                                <div class="row">
                                    <div class="col-lg-5 col-md-5 col-pad">
                                        <div class="car-thumbnail">
                                            <a href="{{ route('car.detail', $car->id) }}" class="car-img">
                                                <img src="{{ asset($car->carImages->first()->image_url) }}" alt="car"
                                                     class="img-fluid">
                                                <div class="tag">{{ $car->status }}</div>
                                                <div class="price-box">{{strrev(wordwrap(strrev($car->price), 3, '.', true))}} F CFA</div>
                                            </a>
                                            <div class="carbox-overlap-wrapper">
                                                <div class="overlap-box">
                                                    <div class="overlap-btns-area">
                                                        <a class="overlap-btn" data-toggle="modal"
                                                           data-target="{{ '#carOverviewModal'.$car->id }}">
                                                            <i class="fa fa-eye-slash"></i>
                                                        </a>
                                                        <a class="overlap-btn wishlist-btn">
                                                            <i class="fa fa-heart-o"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Car Overview Modal -->
                                            <div class="car-model-2">
                                                <div class="modal fade" id="{{'carOverviewModal'.$car->id}}"
                                                     tabindex="-1" role="dialog"
                                                     aria-labelledby="carOverviewModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="carOverviewModalLabel">
                                                                    {{ __('Find Your Dream Car') }}
                                                                </h5>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row modal-raw">
                                                                    <div class="col-lg-5 modal-left">
                                                                        <div class="modal-left-content">
                                                                            <div class="bs-example"
                                                                                 data-example-id="carousel-with-captions">
                                                                                <div class="carousel slide"
                                                                                     id="properties-carousel"
                                                                                     data-ride="carousel">
                                                                                    <div class="carousel-inner"
                                                                                         role="listbox">
                                                                                        <div class="item active">
                                                                                            <img
                                                                                                src="{{ asset($car->carImages->first()->image_url) }}"
                                                                                                alt="best-car"
                                                                                                class="img-fluid">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="description">
                                                                                <h3>Description</h3>
                                                                                <p>{{ \Illuminate\Support\Str::limit($car->description, 130) }}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-7 modal-right">
                                                                        <div class="modal-right-content">
                                                                            <section>
                                                                                <h3>{{__('Overview')}}</h3>
                                                                                <dl>
                                                                                    <dt>{{ __('Model') }}</dt>
                                                                                    <dd>{{ $car->model }}</dd>
                                                                                    <dt>{{__('Fuel Type')}}</dt>
                                                                                    <dd>{{ $car->fuel_type }}</dd>
                                                                                    @if($car->body_style)
                                                                                        <dt>{{__('Body Style')}}</dt>
                                                                                        <dd>{{ $car->body_style }}</dd>
                                                                                    @endif
                                                                                    <dt>{{__('Make')}}</dt>
                                                                                    <dd>{{ $car->make }}</dd>
                                                                                    <dt>{{__('State')}}</dt>
                                                                                    <dd>{{ $car->state }}</dd>
                                                                                    <dt>{{__('Brand')}}</dt>
                                                                                    <dd>{{ $car->brand }}</dd>
                                                                                    <dt>{{__('Mileage')}}</dt>
                                                                                    <dd>{{ $car->mileage }}</dd>
                                                                                    <dt>{{__('Year')}}</dt>
                                                                                    <dd>{{ $car->year }}</dd>
                                                                                    <dt>{{__('Price')}}</dt>
                                                                                    <dd>{{strrev(wordwrap(strrev($car->price), 3, '.', true))}} F CFA</dd>
                                                                                </dl>
                                                                            </section>
                                                                            {{--                                                                            <div class="price-box-2"><sup>$</sup>780<span>/month</span></div>--}}
                                                                            <div class="dd">
                                                                                {{-- <div class="float-left">
                                                                                     <div class="ratings-2">
                                                                                         <span class="ratings-box">4.5/5</span>
                                                                                         <i class="fa fa-star"></i>
                                                                                         <i class="fa fa-star"></i>
                                                                                         <i class="fa fa-star"></i>
                                                                                         <i class="fa fa-star"></i>
                                                                                         <i class="fa fa-star-o"></i>
                                                                                         <span>( 7 Reviews )</span>
                                                                                     </div>
                                                                                 </div>--}}
                                                                                <div class="float-right">
                                                                                    <a href="{{ route('car.detail', $car->id) }}"
                                                                                       class="btn btn-md btn-round btn-theme">{{__('Show Detail')}}</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-pad align-self-center">
                                        <div class="detail">
                                            <h3 class="title">
                                                <a href="{{ route('car.detail', $car->id) }}">{{ $car->brand }}</a>
                                            </h3>
                                            <h5 class="location">
                                                <a href="{{ route('car.detail', $car->id) }}" style="color: var(--BLUE_COLOR)">
                                                    <i class="fa fa-registered"></i> {{ $car->reference }},
                                                </a>
                                                <a href="{{ route('car.detail', $car->id) }}">
                                                    <i class="flaticon-pin"></i>{{ $car->localisation }}
                                                </a>
                                            </h5>
                                            <ul class="facilities-list clearfix">
                                                <li>
                                                    <i class="flaticon-way"></i> {{ $car->mileage }} km
                                                </li>
                                                @if($car->transmission )
                                                    <li>
                                                        <i class="flaticon-manual-transmission"></i> {{ $car->transmission }}
                                                    </li>
                                                @endif
                                                <li>
                                                    <i class="flaticon-calendar-1"></i> {{ $car->year }}
                                                </li>
                                                <li>
                                                    <i class="flaticon-fuel"></i>{{ $car->fuel_type }}
                                                </li>
                                                <li>
                                                    <i class="flaticon-car"></i> {{ $car->type }}
                                                </li>
                                                @if($car->body_style )
                                                    <li>
                                                        <i class="flaticon-gear"></i> {{ $car->body_style }}
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    <!-- Page navigation start -->
                        <div class="pagination-box text-center">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    {{ $cars->links() }}
                                </ul>
                            </nav>
                        </div>
                        @else
                            <div
                                class="alert alert-warning alert-block {{--col-lg-offset-3 col-lg-6 col-lg-offset-3--}}">
                                <strong>Désolé aucune voiture ne correspond à votre recherche !!! </strong>
                            </div>
                    </div>
                @endif
            @else
                <div class="alert alert-warning alert-block col-lg-4 col-md-12">
                    <strong>Désolé aucune voiture n'est disponible !!! </strong>
                </div>
            @endif
        </div>
    </div>
</div>
<!-- Featured car end -->


<!-- Advantages 3 start -->
<div class="advantages-3">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 align-self-center">
                <!-- Main title -->
                <div class="main-title">
                    <h1>{{__('Our Advantages')}}</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
                    <a href="#" class="btn border-thn contact-btn">Contact us</a>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="media advantages-box-3">
                            <div class="icon">
                                <i class="flaticon-shield"></i>
                            </div>
                            <div class="detail media-body align-self-center">
                                <h5>Highly Secured</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt
                                    aliquam.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="media advantages-box-3">
                            <div class="icon">
                                <i class="flaticon-deal"></i>
                            </div>
                            <div class="detail media-body align-self-center">
                                <h5>Trusted Agents</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt
                                    aliquam.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="media advantages-box-3">
                            <div class="icon">
                                <i class="flaticon-money"></i>
                            </div>
                            <div class="detail media-body align-self-center">
                                <h5>Get an Offer</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt
                                    aliquam.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="media advantages-box-3">
                            <div class="icon">
                                <i class="flaticon-support-2"></i>
                            </div>
                            <div class="detail media-body align-self-center">
                                <h5>Free Support</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt
                                    aliquam.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Advantages 3 end -->

<!-- Latest offers Start -->
<div class="latest-offers categories content-area-13">
    <div class="container">
        <!-- Main title -->
        <div class="main-title text-center">
            <h1>{{__('Latest Offers')}}</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
        </div>
        <div class="row row-2">
            <div class="col-lg-4 col-md-12">
                <div class="row">
                    <div class="col-md-6 col-lg-12 col-pad">
                        <div class="category">
                            <div class="category_bg_box cat-1-bg">
                                <div class="category-overlay">
                                    <div class="category-content">
                                        <div class="new">New</div>
                                        <div class="lo-text clearfix">
                                            <h3 class="category-title">
                                                <a href="../car/car-details.html">Toyota Prius specs</a>
                                            </h3>
                                            <h5 class="category-subtitle">850.00 <span>/Monthly</span></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-12 col-pad">
                        <div class="category">
                            <div class="category_bg_box cat-3-bg">
                                <div class="category-overlay">
                                    <div class="category-content">
                                        <div class="new">New</div>
                                        <div class="lo-text clearfix">
                                            <h3 class="category-title">
                                                <a href="../car/car-details.html">Toyota Prius specs</a>
                                            </h3>
                                            <h5 class="category-subtitle">430.00 <span>/Monthly</span></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-pad">
                <div class="category">
                    <div class="category_bg_box category_long_bg cat-4-bg">
                        <div class="category-overlay">
                            <div class="category-overlay">
                                <div class="category-content">
                                    <div class="new">New</div>
                                    <div class="lo-text clearfix">
                                        <h3 class="category-title">
                                            <a href="../car/car-details.html">Audi R8</a>
                                        </h3>
                                        <h5 class="category-subtitle">$920.00 <span>/Monthly</span></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-pad">
                        <div class="category">
                            <div class="category_bg_box cat-2-bg">
                                <div class="category-overlay">
                                    <div class="category-content">
                                        <div class="new">New</div>
                                        <div class="lo-text clearfix">
                                            <h3 class="category-title">
                                                <a href="../car/car-details.html">Toyota Prius</a>
                                            </h3>
                                            <h5 class="category-subtitle">540.00 <span>/Monthly</span></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-pad">
                        <div class="category">
                            <div class="category_bg_box cat-5-bg">
                                <div class="category-overlay">
                                    <div class="category-content">
                                        <div class="new">New</div>
                                        <div class="lo-text clearfix">
                                            <h3 class="category-title">
                                                <a href="../car/car-details.html">2017 Ford Mustang</a>
                                            </h3>
                                            <h5 class="category-subtitle">860.00 <span>/Monthly</span></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Latest offers end -->


<!-- Footer start -->
<footer class="footer">
    <div class="container footer-inner">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="footer-item clearfix">
                    <img src="{{ asset('template/home_page/img/logos/logo.png') }}" alt="logo" class="f-logo">
                    <ul class="contact-info">
                        <li>
                            <i class="flaticon-pin"></i>20/F Green Road, Dhanmondi, Dhaka
                        </li>
                        <li>
                            <i class="flaticon-mail"></i><a href="mailto:sales@hotelempire.com">info@themevessel.com</a>
                        </li>
                        <li>
                            <i class="flaticon-phone"></i><a href="tel:+55-417-634-7071">+0477 85X6 552</a>
                        </li>
                        <li>
                            <i class="flaticon-fax"></i>+0477 85X6 552
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="social-list-2">
                        <ul>
                            <li><a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" class="google-bg"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" class="linkedin-bg"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6">
                <div class="footer-item">
                    <h4>
                        Useful Links
                    </h4>
                    <ul class="links">
                        <li><a href="#">Home</a></li>
                        <li>
                            <a href="#">About Us</a>
                        </li>
                        <li>
                            <a href="#">Services</a>
                        </li>
                        <li>
                            <a href="#">Faq</a>
                        </li>
                        <li>
                            <a href="#">Contact Us</a>
                        </li>
                        <li>
                            <a href="#">Car Compare</a>
                        </li>
                        <li>
                            <a href="#">Car Reviews</a>
                        </li>
                        <li>
                            <a href="#">Elements</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <div class="recent-posts footer-item">
                    <h4>Recent Posts</h4>
                    <div class="media mb-4">
                        <a class="pr-3" href="#">
                            <img class="media-object" src="{{ asset('template/home_page/img/car/small-car-3.jpg') }}"
                                 alt="small-car">
                        </a>
                        <div class="media-body align-self-center">
                            <h5>
                                <a href="#">Bentley Continental GT</a>
                            </h5>
                            <div class="listing-post-meta">
                                $345,00 | <a href="#"><i class="fa fa-calendar"></i> Jan 12, 2019</a>
                            </div>
                        </div>
                    </div>
                    <div class="media mb-4">
                        <a class="pr-3" href="#">
                            <img class="media-object" src="{{ asset('template/home_page/img/car/small-car-1.jpg') }}"
                                 alt="small-car">
                        </a>
                        <div class="media-body align-self-center">
                            <h5>
                                <a href="#">Fiat Punto Red</a>
                            </h5>
                            <div class="listing-post-meta">
                                $745,00 | <a href="#"><i class="fa fa-calendar"></i> Feb 26, 2019</a>
                            </div>
                        </div>
                    </div>
                    <div class="media">
                        <a class="pr-3" href="#">
                            <img class="media-object" src="{{ asset('template/home_page/img/car/small-car-2.jpg') }}"
                                 alt="small-car">
                        </a>
                        <div class="media-body align-self-center">
                            <h5>
                                <a href="#">Nissan Micra Gen5</a>
                            </h5>
                            <div class="listing-post-meta">
                                $745,00 | <a href="#"><i class="fa fa-calendar"></i> Feb 14, 2019</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="footer-item clearfix">
                    <h4>{{__('Subscribe')}}</h4>
                    <div class="Subscribe-box">
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                            mollit.</p>
                        <form class="form-inline" action="#" method="GET">
                            <input type="text" class="form-control mb-sm-0" id="inlineFormInputName3"
                                   placeholder="{{__('Email Address')}}">
                            <button type="submit" class="btn"><i class="fa fa-paper-plane"></i></button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xl-12 col-lg-12">
                <p class="copy sub-footer">© 2019 <a href="#">Theme Vessel.</a> Trademarks and brands are the property
                    of their respective owners.</p>
            </div>
        </div>
    </div>
</footer>
<!-- Footer end -->

<!-- Full Page Search -->
<div id="full-page-search">
    <button type="button" class="close">×</button>
    <form action="http://storage.googleapis.com/themevessel-products/cmart/index.html#">
        <input type="search" value="" placeholder="type keyword(s) here"/>
        <button type="submit" class="btn btn-sm button-theme">Search</button>
    </form>
</div>

<!-- Car Overview Modal -->
<script src="{{ asset('template/home_page/js/jquery-2.2.0.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/popper.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/bootstrap-submenu.js') }}"></script>
<script src="{{ asset('template/home_page/js/rangeslider.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.mb.YTPlayer.js') }}"></script>
<script src="{{ asset('template/home_page/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.easing.1.3.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.scrollUp.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/leaflet.js') }}"></script>
<script src="{{ asset('template/home_page/js/leaflet-providers.js') }}"></script>
<script src="{{ asset('template/home_page/js/leaflet.markercluster.js') }}"></script>
<script src="{{ asset('template/home_page/js/dropzone.js') }}"></script>
<script src="{{ asset('template/home_page/js/slick.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.filterizr.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.countdown.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.mousewheel.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/lightgallery-all.js') }}"></script>
<script src="{{ asset('template/home_page/js/jnoty.js') }}"></script>
<script src="{{ asset('template/home_page/js/maps.js') }}"></script>
<script src="{{ asset('template/home_page/js/app.js') }}"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="{{ asset('template/home_page/js/ie10-viewport-bug-workaround.js') }}"></script>
<!-- Custom javascript -->
<script src="{{ asset('template/home_page/js/ie10-viewport-bug-workaround.js') }}"></script>
</body>

<!-- Mirrored from storage.googleapis.com/themevessel-products/cmart/car-list-leftsidebar.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Apr 2020 10:20:47 GMT -->
</html>
