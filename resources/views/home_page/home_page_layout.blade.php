<!DOCTYPE html>
<html lang="zxx">

<!-- Mirrored from storage.googleapis.com/themevessel-products/cmart/car-list-leftsidebar.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Apr 2020 10:20:47 GMT -->
<head>
    <title>228 - Car Import</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/bootstrap-submenu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/magnific-popup.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/leaflet.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/map.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('template/home_page/fonts/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/fonts/flaticon/font/flaticon.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/fonts/linearicons/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/jquery.mCustomScrollbar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/dropzone.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/lightbox.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/jnoty.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/slick.css') }}">

    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/style.css') }}">
    <link rel="stylesheet" type="text/css" id="style_sheet"
          href="{{ asset('template/home_page/css/skins/midnight-blue.css') }}">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{ asset('template/home_page/img/favicon.ico') }}" type="image/x-icon">
{{--    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >--}}

<!-- Google fonts -->
    <link href="{{ asset('template/home_page/fonts/google/googleapis.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/fonts/google/googleapis_raleway.css') }}">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('template/home_page/css/ie10-viewport-bug-workaround.css') }}">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script  src="{{ asset('template/home_page/js/ie8-responsive-file-warning.js') }}"></script><![endif]-->
    <script src="{{ asset('template/home_page/js/ie-emulation-modes-warning.js') }}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script  src="{{ asset('template/home_page/js/html5shiv.min.js') }}"></script>
    <script  src="{{ asset('template/home_page/js/respond.min.js') }}"></script>
    <![endif]-->
</head>
<body>
<div class="page_loader"></div>

<!-- Main header start -->
<header class="main-header sticky-header header-with-top">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand company-logo" href="{{ route('home.page') }}">
                <img src="{{ asset('template/home_page/img/logos/black-logo.png') }}" alt="logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
                    aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fa fa-bars"></span>
            </button>
            <div class="navbar-collapse collapse w-100" id="navbar">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="{{ route('home.page') }}">
                            {{ __('Home') }}
                        </a>
                    </li>
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="{{ route('home.page') . '#lists' }}">
                            {{ __('Cars lists') }}
                        </a>
                    </li>
                    <li class="nav-item dropdown megamenu-li">
                        <a class="nav-link dropdown-toggle" href="{{ route('home.page') . '#benefits' }}">
                            {{ __('Benefits') }}
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="{{ route('home.page') . '##latest_offers' }}">
                            {{ __('Latest Offers') }}
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#">Contact</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
<!-- Main header end -->

@yield('content')

<!-- Footer start -->
<footer class="footer">
    <div class="container footer-inner">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="footer-item clearfix">
                    <img src="{{ asset('template/home_page/img/logos/logo.png') }}" alt="logo" class="f-logo">
                    <ul class="contact-info">
                        <li>
                            <i class="flaticon-pin"></i>20/F Green Road, Dhanmondi, Dhaka
                        </li>
                        <li>
                            <i class="flaticon-mail"></i><a href="mailto:sales@hotelempire.com">info@themevessel.com</a>
                        </li>
                        <li>
                            <i class="flaticon-phone"></i><a href="tel:+55-417-634-7071">+0477 85X6 552</a>
                        </li>
                        <li>
                            <i class="flaticon-fax"></i>+0477 85X6 552
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="social-list-2">
                        <ul>
                            <li><a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" class="google-bg"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" class="linkedin-bg"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6">
                <div class="footer-item">
                    <h4>
                        Useful Links
                    </h4>
                    <ul class="links">
                        <li><a href="#">Home</a></li>
                        <li>
                            <a href="#">About Us</a>
                        </li>
                        <li>
                            <a href="#">Services</a>
                        </li>
                        <li>
                            <a href="#">Faq</a>
                        </li>
                        <li>
                            <a href="#">Contact Us</a>
                        </li>
                        <li>
                            <a href="#">Car Compare</a>
                        </li>
                        <li>
                            <a href="#">Car Reviews</a>
                        </li>
                        <li>
                            <a href="#">Elements</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <div class="recent-posts footer-item">
                    <h4>Recent Posts</h4>
                    <div class="media mb-4">
                        <a class="pr-3" href="#">
                            <img class="media-object" src="{{ asset('template/home_page/img/car/small-car-3.jpg') }}"
                                 alt="small-car">
                        </a>
                        <div class="media-body align-self-center">
                            <h5>
                                <a href="#">Bentley Continental GT</a>
                            </h5>
                            <div class="listing-post-meta">
                                $345,00 | <a href="#"><i class="fa fa-calendar"></i> Jan 12, 2019</a>
                            </div>
                        </div>
                    </div>
                    <div class="media mb-4">
                        <a class="pr-3" href="#">
                            <img class="media-object" src="{{ asset('template/home_page/img/car/small-car-1.jpg') }}"
                                 alt="small-car">
                        </a>
                        <div class="media-body align-self-center">
                            <h5>
                                <a href="#">Fiat Punto Red</a>
                            </h5>
                            <div class="listing-post-meta">
                                $745,00 | <a href="#"><i class="fa fa-calendar"></i> Feb 26, 2019</a>
                            </div>
                        </div>
                    </div>
                    <div class="media">
                        <a class="pr-3" href="#">
                            <img class="media-object" src="{{ asset('template/home_page/img/car/small-car-2.jpg') }}"
                                 alt="small-car">
                        </a>
                        <div class="media-body align-self-center">
                            <h5>
                                <a href="#">Nissan Micra Gen5</a>
                            </h5>
                            <div class="listing-post-meta">
                                $745,00 | <a href="#"><i class="fa fa-calendar"></i> Feb 14, 2019</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="footer-item clearfix">
                    <h4>{{__('Subscribe')}}</h4>
                    <div class="Subscribe-box">
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                            mollit.</p>
                        <form class="form-inline" action="#" method="GET">
                            <input type="text" class="form-control mb-sm-0" id="inlineFormInputName3"
                                   placeholder="{{__('Email Address')}}">
                            <button type="submit" class="btn"><i class="fa fa-paper-plane"></i></button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xl-12 col-lg-12">
                <p class="copy sub-footer">© 2019 <a href="#">Theme Vessel.</a> Trademarks and brands are the property
                    of their respective owners.</p>
            </div>
        </div>
    </div>
</footer>
<!-- Footer end -->

<!-- Full Page Search -->
<div id="full-page-search">
    <button type="button" class="close">×</button>
    <form action="http://storage.googleapis.com/themevessel-products/cmart/index.html#">
        <input type="search" value="" placeholder="type keyword(s) here"/>
        <button type="submit" class="btn btn-sm button-theme">Search</button>
    </form>
</div>

<!-- Car Overview Modal -->
<script src="{{ asset('template/home_page/js/jquery-2.2.0.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/popper.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/bootstrap-submenu.js') }}"></script>
<script src="{{ asset('template/home_page/js/rangeslider.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.mb.YTPlayer.js') }}"></script>
<script src="{{ asset('template/home_page/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.easing.1.3.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.scrollUp.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/leaflet.js') }}"></script>
<script src="{{ asset('template/home_page/js/leaflet-providers.js') }}"></script>
<script src="{{ asset('template/home_page/js/leaflet.markercluster.js') }}"></script>
<script src="{{ asset('template/home_page/js/dropzone.js') }}"></script>
<script src="{{ asset('template/home_page/js/slick.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.filterizr.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.countdown.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.mousewheel.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/lightgallery-all.js') }}"></script>
<script src="{{ asset('template/home_page/js/jnoty.js') }}"></script>
<script src="{{ asset('template/home_page/js/maps.js') }}"></script>
<script src="{{ asset('template/home_page/js/app.js') }}"></script>

@yield('scripts')

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="{{ asset('template/home_page/js/ie10-viewport-bug-workaround.js') }}"></script>
<!-- Custom javascript -->
<script src="{{ asset('template/home_page/js/ie10-viewport-bug-workaround.js') }}"></script>
</body>

<!-- Mirrored from storage.googleapis.com/themevessel-products/cmart/car-list-leftsidebar.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Apr 2020 10:20:47 GMT -->
</html>
