<!-- Brand Field -->
<div class="row offset-sm-1 col-sm-10 offset-sm-1">
    <div class="form-group col-sm-12">
        <h6>{!! Form::label('brand', __('Brand:')) !!}</h6>
        {!! Form::text('brand', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Make Field -->
    <div class="form-group col-sm-12">
        <h6>{!! Form::label('make', __('Make:')) !!}</h6>
        {!! Form::text('make', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Model Field -->
    <div class="form-group col-sm-6">
        <h6>{!! Form::label('model', __('Model:')) !!}</h6>
        {!! Form::text('model', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Fuel Type Field -->
    <div class="form-group col-sm-6">
        <h6>{!! Form::label('fuel_type', __('Fuel Type:')) !!}</h6>
        {!! Form::text('fuel_type', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Engine Field -->
    <div class="form-group col-sm-12">
        <h6>{!! Form::label('engine', __('Engine:')) !!}</h6>
        {!! Form::text('engine', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Mileage Field -->
    <div class="form-group col-sm-6">
        <h6>{!! Form::label('mileage', __('Mileage:')) !!}</h6>
        {!! Form::text('mileage', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Transmission Field -->
    <div class="form-group col-sm-6">
        <h6>{!! Form::label('transmission', __('Transmission:') . ' (' .__('optional') . ')' ) !!}</h6>
        {!! Form::select('transmission', ['Automatique' => 'Automatique', 'Manuelle' => 'Manuelle'], null,
        ['class' => 'form-control', 'placeholder' => 'Sélectionner la transmission']) !!}
    </div>

    <!-- Gears Num Field -->
    <div class="form-group col-sm-6">
        <h6>{!! Form::label('gears_num', __('Gears Num:') . ' (' .__('optional') . ')' ) !!}</h6>
        {!! Form::number('gears_num', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Body Style Field -->
    <div class="form-group col-sm-6">
        <h6>{!! Form::label('body_style', __('Body Style:') . ' (' .__('optional') . ')' ) !!}</h6>
        {!! Form::text('body_style', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Type Field -->
    <div class="form-group col-sm-6">
        <h6>{!! Form::label('type', __('Type:')) !!}</h6>
        {!! Form::text('type', null, ['class' => 'form-control']) !!}
    </div>

    <!-- State Field -->
    <div class="form-group col-sm-6">
        <h6>{!! Form::label('state', __('State:')) !!}
            <a class="info" data-tooltip="Sélectionner l'état de la voiture : Nouvelle voiture ou Ancienne voiture."><i class="fa fa-question-circle fa-lg"></i></a>
        </h6>
        {!! Form::select('state', ['Nouvelle voiture' => 'Nouvelle voiture', 'Voiture Utilisée' => 'Ancienne voiture'],
            null, ['class' => 'form-control', 'placeholder' => 'Selectionner l\'état']) !!}
    </div>

    <!-- Year Field -->
    <div class="form-group col-sm-6">
        <h6>{!! Form::label('year', __('Year:')) !!}
            <a class="info" data-tooltip="Année de Fabrication de la voiture."><i class="fa fa-question-circle fa-lg"></i></a>
        </h6>
        {!! Form::select('year', $years, null, ['class' => 'form-control']) !!}
    </div>

    <!-- Price Field -->
    <div class="form-group col-sm-6">
        <h6>{!! Form::label('price', __('Price:')) !!}</h6>
        {!! Form::number('price', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Localisation Field -->
    <div class="form-group col-sm-6">
        <h6>{!! Form::label('localisation', __('Localisation:')) !!}
            <a class="info" data-tooltip="Saisir la localisation actuelle de la voiture."><i class="fa fa-question-circle fa-lg"></i></a>
        </h6>
        {!! Form::text('localisation', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Status Field -->
    <div class="form-group col-sm-6">
        <h6>{!! Form::label('status', __('Status:')) !!}
            <a class="info" data-tooltip="Sélectionner si la voiture est déjà vendue ou pas."><i class="fa fa-question-circle fa-lg"></i></a>
        </h6>
        {!! Form::select('status', ['Vendue' => 'Vendue', 'À Vendre' => 'À Vendre'],
            null, ['class' => 'form-control', 'placeholder' => 'Sélectionner le statut']) !!}
    </div>

    <!-- Seller Field -->
    <div class="form-group col-sm-6">
        <h6>{!! Form::label('seller', __('Seller:')) !!}
            <a class="info" data-tooltip="Sélectionner le vendeur de la voiture. Celui que les clients peuvent contacter."><i class="fa fa-question-circle fa-lg"></i></a>
        </h6>
        {!! Form::select('seller_id', $sellers, null, ['class' => 'form-control', 'placeholder' => 'Sélectionner le vendeur']) !!}
    </div>

    <!-- Visibility Field -->
    <div class="form-group col-sm-6">
        <h6>{!! Form::label('visibility', __('Visibility:')) !!}</h6>
        {!! Form::hidden('visibility', 0) !!}
        {!! Form::checkbox('visibility', '1', null,
            ['data-toggle' => 'toggle', 'data-on'=> __('On'), 'data-off' => __('Off'), 'data-onstyle' => 'success', 'data-offstyle'=>'danger'])
        !!}
    </div>

    <!-- Description Field -->
    <div class="form-group col-sm-12 col-lg-12">
        <h6>{!! Form::label('description', 'Description :') !!}</h6>
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    </div>

    @if(isset($edit))
        @include('admin.cars.car_images')
        <div class="form-group col-sm-12">
            <h6>{!! Form::label('image', 'Image :' . ' (' . __('optional') . ')' ) !!}
                <a class="info" data-tooltip="Utiliser ce champ pour charger les images correspondants à la voiture."><i class="fa fa-question-circle fa-lg"></i></a>
            </h6>
            {!! Form::file('images[]', ['id' => 'images', 'class' => 'form-control', 'accept' => 'image/*', 'multiple' => true]) !!}
        </div>
    @else
        <div class="form-group col-sm-12">
            <h6>{!! Form::label('image', 'Image :') !!}
                <a class="info" data-tooltip="Utiliser ce champ pour charger les images correspondants à la voiture."><i class="fa fa-question-circle fa-lg"></i></a>
            </h6>
            {!! Form::file('images[]', ['id' => 'images', 'class' => 'form-control', 'accept' => 'image/*', 'multiple' => true, 'required']) !!}
        </div>
    @endif

<!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit(__('Save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{ route('admin.cars.index') }}" class="btn btn-secondary">{{__('Cancel')}}</a>
    </div>

</div>

@section('styles')
    <link rel="stylesheet" href="{{ asset('template/admin/css/info.css') }}">
    <link rel="stylesheet" href="{{ asset('template/admin/css/selectize.bootstrap3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/admin/css/bootstrap4-toggle.min.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('template/admin/js/selectize.min.js') }}"></script>
    <script src="{{ asset('template/admin/js/bootstrap4-toggle.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('select').selectize({
                sortField: 'text'
            });
        });
    </script>
    <script>
        $(".delete-image").click(function (e) {
            //e.preventDefault();
            var ele = $(this);
            Swal.fire({
                title: "{{__('Are you sure?') }}",
                html: "L'image sera supprimée définitivement !",
                width: 400,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: "{{ __('Yes') }}",
                cancelButtonText: "{{ __('Cancel') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '{{ route('admin.car.delete.image') }}',
                        method: "delete",
                        data: {_token: '{{ csrf_token() }}', id: ele.attr("id")},
                        success: function (response) {
                            window.location.reload();
                        }
                    });
                }
            });
        });
    </script>
@endsection
