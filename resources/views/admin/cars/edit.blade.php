@extends('admin.layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('admin.cars.index') !!}">{{ __('Cars') }}</a>
          </li>
          <li class="breadcrumb-item active">Editer une Voiture</li>
    </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <h3><i class="fa fa-edit fa-lg"></i> Editer une Voiture</h3>
                          </div>
                          <div class="card-body">
                              {!! Form::model($car, ['route' => ['admin.cars.update', $car->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                              @include('admin.cars.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection
