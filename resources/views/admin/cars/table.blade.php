<div class="table-responsive-sm">
    <table class="table table-striped" id="cars-table">
        <thead>
            <tr>
                <th>Brand</th>
                <th>Make</th>
                <th>Model</th>
                <th>Fuel Type</th>
                <th>Engine</th>
                <th>Mileage</th>
                <th>Transmission</th>
                <th>Gears Num</th>
                <th>Body Style</th>
                <th>Type</th>
                <th>State</th>
                <th>Year</th>
                <th>Price</th>
                <th>Localisation</th>
                <th>Status</th>
                <th>Description</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($cars as $car)
            <tr>
                <td>{{ $car->brand }}</td>
                <td>{{ $car->make }}</td>
                <td>{{ $car->model }}</td>
                <td>{{ $car->fuel_type }}</td>
                <td>{{ $car->engine }}</td>
                <td>{{ $car->mileage }}</td>
                <td>{{ $car->transmission }}</td>
                <td>{{ $car->gears_num }}</td>
                <td>{{ $car->body_style }}</td>
                <td>{{ $car->type }}</td>
                <td>{{ $car->state }}</td>
                <td>{{ $car->year }}</td>
                <td>{{ $car->price }}</td>
                <td>{{ $car->localisation }}</td>
                <td>{{ $car->status }}</td>
                <td>{{ $car->description }}</td>
                <td>
                    {!! Form::open(['route' => ['admin.cars.destroy', $car->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('admin.cars.show', [$car->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('admin.cars.edit', [$car->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
