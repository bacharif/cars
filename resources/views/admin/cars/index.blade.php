@extends('admin.layouts.app')

@section('styles')
    <!-- Datatables Js-->
    <link rel="stylesheet" type="text/css" href="{{ asset('template/admin/css/datatables.min.css') }}"/>
@endsection

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><h2><b>Liste des Voitures</b></h2></li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="search">
                             <div class="card-header" style="padding-bottom: 25px;">
                                 <i class="fa fa-align-justify"></i>
                                 {{__('Cars')}}
                                 <a class="pull-right btn btn-primary" href="{{ route('admin.cars.create') }}">
                                     <i class="fa fa-plus fa-lg"></i> <b>Publier une voiture</b>
                                 </a>
                             </div>
                             <div class="card-body">
                                 {{--@include('admin.cars.table')--}}
                                 <div class="table-responsive-sm">
                                     <table id="table" class="table table-bordered table-striped">
                                         <thead>
                                         <tr>
                                             <th>{{__('Image')}}</th>
                                             <th>{{__('Reference')}}</th>
                                             <th>{{__('Brand')}}</th>
                                             <th>{{__('Model')}}</th>
                                             <th>{{__('Fuel Type')}}</th>
                                             <th>{{__('Year')}}</th>
                                             <th>{{__('Price')}}</th>
                                             <th>{{__('Status')}}</th>
                                             <th>{{__('Actions')}}</th>
                                         </tr>
                                         </thead>
                                         <tbody id="table_contents">
                                            @foreach($cars as $car)
                                                <tr class="row1" data-id="{{ $car->id }}">
                                                    <td>
                                                        <img src="{{ $car->carImages->first()->image_url }}" alt="Car Image" style="width: 110px; height: 70px">
                                                    </td>
                                                    <td>{{ $car->reference }}</td>
                                                    <td>{{ $car->brand }}</td>
                                                    <td>{{ $car->model }}</td>
                                                    <td>{{ $car->fuel_type }}</td>
                                                    <td>{{ $car->year }}</td>
                                                    <td>{{ $car->price }}</td>
                                                    <td>{{ $car->status }}</td>
                                                    <td>
                                                        {!! Form::open(['route' => ['admin.cars.destroy', $car->id], 'method' => 'delete']) !!}
                                                        <div class='btn-group'>
                                                            <a href="{{ route('admin.cars.show', [$car->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                                                            <a href="{{ route('admin.cars.edit', [$car->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                                                            {!! Form::button('<i class="fa fa-trash"></i>',
                                                                ['type' => 'submit', 'class' => 'btn btn-ghost-danger'])
                                                            !!}
                                                        </div>
                                                        {!! Form::close() !!}
                                                    </td>
                                                </tr>
                                            @endforeach
                                         </tbody>
                                     </table>
                                 </div>
                                 <div class="pull-right mr-3">

                                 </div>
                             </div>
                         </div>
                     </div>
                  </div>

                 @if($trashed_cars->isNotEmpty())
                     <div class="col-lg-12">
                         <a id="trash" class="btn btn-primary" href="#" style="margin-bottom: 20px">
                             <i class="fa fa-eye"></i> <b>{{__('Trash')}}</b></a>
                     </div>

                     <div class="col-lg-12" id="trashed_cars" style="display: none">
                         <div class="card">
                             <div class="card-header">
                                 <i class="fa fa-align-justify"></i>
                                 Voitures Supprimées
                                 <a class="btn btn-success pull-right" href="{{ route('admin.cars.restore_all') }}">
                                     <i class="fa fa-trash-o"></i> <b>{{__('Restore all')}}</b></a>
                             </div>

                             <div class="card-body">
                                 <div class="table-responsive {{--col-lg-12--}}">
                                     <table class="table table-striped table-bordered">
                                         <thead>
                                         <tr>
                                             <th>{{__('Image')}}</th>
                                             <th>{{__('Brand')}}</th>
                                             <th>{{__('Model')}}</th>
                                             <th>{{__('Fuel Type')}}</th>
                                             <th>{{__('Mileage')}}</th>
                                             <th>{{__('Type')}}</th>
                                             <th>{{__('Year')}}</th>
                                             <th>{{__('Price')}}</th>
                                             <th>{{__('Status')}}</th>
                                             <th>{{ __('Date de Publication') }}</th>
                                             <th>{{__('Actions')}}</th>
                                         </tr>
                                         </thead>
                                         <tbody>
                                         @foreach($trashed_cars as $car)
                                             <tr class="cart_item">
                                                 <td>
                                                     <img src="{{ $car->carImages->first()->image_url }}" style="width: 80px; height: 70px" alt="Car Image">
                                                 </td>
                                                 <td> {{ $car->name }}</td>
                                                 <td> {{ $car->brand }}</td>
                                                 <td> {{ $car->model }}</td>
                                                 <td> {{ $car->fuel_type }}</td>
                                                 <td> {{ $car->mileage }}</td>
                                                 <td> {{ $car->year }}</td>
                                                 <td> {{ $car->price }}</td>
                                                 <td> {{ $car->status }}</td>
                                                 <td> {{ $car->created_at }}</td>
                                                 <td>
                                                     <div>
                                                         <div class='btn-group'>
                                                             <a title="{{__('Restore')}}"
                                                                href="{{ route('admin.car.restore', $car->id) }}"
                                                                class='btn btn-ghost-success'>
                                                                 <i class="fa fa-window-restore"></i>
                                                             </a>
                                                         </div>
                                                     </div>
                                                 </td>
                                             </tr>
                                         @endforeach
                                         </tbody>
                                     </table>
                                 </div>
                             </div>
                         </div>
                     </div>
                 @endif

             </div>
         </div>
    </div>
@endsection

@section('scripts')
    <!-- jQuery UI -->
    <script type="text/javascript" src="{{ asset('template/admin/js/jquery-ui.js') }}" ></script>
    <!-- Datatables Js-->
    <script type="text/javascript" src="{{ asset('template/admin/js/datatables.min.js') }}"></script>

    <script>
        $('#table').DataTable({
            "language" : {
                "url" : "{{ asset('lang/' . config('app.locale') . '.json') }}"
            }
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $("#table").DataTable();

            $("#table_contents").sortable({
                items: "tr",
                cursor: 'move',
                opacity: 0.6,
                update: function () {
                    sendOrderToServer();
                }
            });

            function sendOrderToServer() {

                var positions = [];
                $('tr.row1').each(function (index) {
                    positions.push({
                        id: $(this).attr('data-id'),
                        position: index + 1
                    });
                });

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{ url('admin/car/position/update') }}",
                    data: {
                        positions: positions,
                        _token: '{{csrf_token()}}'
                    }
                });
            }
        });
    </script>
    <script type='text/javascript'>
        $(document).on("submit", "form", function (e) {
            e.preventDefault();
            Swal.fire({
                title: "{{__('Are you sure?') }}",
                html: "{{__('Cette voiture ne sera plus affichée dans vos sélections !') }}",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: "{{ __('Yes') }}",
                cancelButtonText: "{{ __('Cancel') }}",
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    e.currentTarget.submit();
                }
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#trash').click(function () {
                $('#trashed_cars').toggle(500);
            });
        });
    </script>
@endsection
