@extends('admin.layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('admin.cars.index') !!}">{{__('Cars')}}</a>
      </li>
      <li class="breadcrumb-item active">{{__('Create')}}</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                <div class="row">
                    <div class="{{--offset-lg-2--}} col-lg-12 {{--offset-lg-2--}}">
                        @include('coreui-templates::common.errors')
                        <div class="card">
                            <div class="card-header">
                                <h3><i class="fa fa-plus-square-o fa-lg"></i> Publier une Voiture</h3>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'admin.cars.store', 'enctype' => 'multipart/form-data']) !!}

                                   @include('admin.cars.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
