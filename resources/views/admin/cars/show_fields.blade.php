<div class="row" style="margin-top: 20px">
    <div class="col-sm-8">
        <!-- Display car's images -->
        <div id="image" class="form-group carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
                @foreach($car->carImages as $image)
                    <li data-target="#image" data-slide-to="{{ $loop->index }}" class="{{ $loop->index == 0 ? 'active' : '' }}"></li>
                @endforeach
            </ul>
{{--            <div class="carousel-inner border">--}}
            <div class="carousel-inner border">
                @foreach($car->carImages as $image)
                    <div class="{{ $loop->index == 0 ? 'carousel-item  active' : 'carousel-item '}}">
                        <img src="{{ $image->image_url }}" alt="Car Image" width="100%">
                    </div>
                @endforeach
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#image" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
{{--            <a class="carousel-control-next" href="#image" data-slide="next">--}}
            <a class="carousel-control-next" href="#image" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </div>
        <!-- End displaying -->

        @if($car->seller)
        <!-- Display the Seller -->
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>{{ __('Seller Name') }}</th>
                        <th>{{ __('Seller Email') }}</th>
                        <th>{{ __('Seller Phone number') }}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $car->seller->full_name }}</td>
                        <td>
                            @if($car->seller->email)
                                {{ $car->seller->email }}
                            @endif
                        </td>
                        <td>{{ $car->seller->phone_number }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- End Displaying -->
        @endif
    </div>

    <div class="col-sm-4">
        <!-- Brand Field -->
        <div class="table-responsive">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th>{{ __('Brand') }}</th>
                        <td>{{ $car->brand }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Make') }}</th>
                        <td>{{ $car->make }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Model') }}</th>
                        <td>{{ $car->model }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Fuel Type') }}</th>
                        <td>{{ $car->fuel_type }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Engine') }}</th>
                        <td>{{ $car->engine }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Mileage') }}</th>
                        <td>{{ $car->mileage }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Transmission') }}</th>
                        <td>{{ $car->transmission }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Gears Num') }}</th>
                        <td>{{ $car->gears_num }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Body Style') }}</th>
                        <td>{{ $car->body_style }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Type') }}</th>
                        <td>{{ $car->type }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('State') }}</th>
                        <td>{{ $car->state }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Year') }}</th>
                        <td>{{ $car->year }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Price') }}</th>
                        <td>{{ $car->price }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Localisation') }}</th>
                        <td>{{ $car->localisation }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Status') }}</th>
                        <td>{{ $car->status }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-sm-12" style="margin-top: 15px">
        <h1 class="text-center"> Description </h1>
        <p style="line-height: 30px !important; font-size: 16px !important;">{{ $car->description }}</p>
    </div>
</div>

