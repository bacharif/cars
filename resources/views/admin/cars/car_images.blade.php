<!-- Display product's images -->
<div id="image" class="form-group col-sm-12 carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ul class="carousel-indicators">
        @foreach($car->carImages as $image)
            <li data-target="#image" data-slide-to="{{ $loop->index }}" class="{{ $loop->index == 0 ? 'active' : '' }}"></li>
        @endforeach
    </ul>
    <!-- The slideshow -->
    <div class="carousel-inner border">
        @foreach($car->carImages as $image)
            <div class="{{ $loop->index == 0 ? 'carousel-item  active' : 'carousel-item '}}">
                <div class="dropdown" href="#">
                    <a data-toggle="dropdown">
                        <img src="{{ $image->image_url }}" alt="Product Image" width="1100" height="500" style="width: 100%; height: 100%">
                    </a>
                    @if(count($car->carImages) > 1)
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item delete-image" id="{{ $image->id }}" style="cursor: pointer; background: #de4350; color: #fff3cd">
                                <i class="fa fa-trash fa-lg"></i> <b>{{__('Delete image')}}</b>
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#image" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#image" data-slide="next">
        <span class="carousel-control-next-icon"></span>
    </a>
</div>
<!-- End displaying -->
