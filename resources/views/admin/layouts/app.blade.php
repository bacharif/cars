<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title>228 - Car -- Admin</title>
    <meta name="description" content="CoreUI Template - InfyOm Laravel Generator">
    <meta name="keyword" content="CoreUI,Bootstrap,Admin,Template,InfyOm,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <!-- Bootstrap-->
    <link rel="stylesheet" href="{{ asset('template/admin/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/admin/css/bootstrap-datetimepicker.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('template/admin/css/coreui.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('template/admin/css/coreui-icons-free.css') }}">
    <link rel="stylesheet" href="{{ asset('template/admin/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/admin/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('template/admin/css/flag-icon.min.css') }}">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{ asset('template/home_page/img/favicon.ico') }}" type="image/x-icon">

     <!-- PRO version // if you have PRO version licence than remove comment and use it. -->
    {{--<link rel="stylesheet" href="https://unpkg.com/@coreui/icons@1.0.0/css/brand.min.css">--}}
    {{--<link rel="stylesheet" href="https://unpkg.com/@coreui/icons@1.0.0/css/flag.min.css">--}}
     <!-- PRO version -->
    <link href="{{ asset('template/admin/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/admin/css/simple-line-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('template/admin/css/flag-icon.min.css') }}" rel="stylesheet">
    <style>
        input, select, textarea {border: 2px solid #ccc !important;}
        /*input {height: 40px !important;}*/
    </style>
    @yield('styles')
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">
        <img class="navbar-brand-full" src="{{ asset('template/home_page/img/logos/black-logo.png') }}" width="80" height="50"
             alt="228Cars Logo">
        <img class="navbar-brand-minimized" src="{{ asset('template/home_page/img/logos/black-logo.png') }}" width="80" height="50"
             alt="228Cars Logo">
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>

    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item d-md-down-none">
            <a class="nav-link" href="#">
                <i class="icon-bell"></i>
                <span class="badge badge-pill badge-danger">5</span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link" style="margin-right: 10px" data-toggle="dropdown" href="#" role="button"
               aria-haspopup="true" aria-expanded="false">
                <strong>{{ Auth::user()->full_name }}</strong>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center">
                    <strong>Account</strong>
                </div>
                <a class="dropdown-item" href="#">
                    <i class="fa fa-envelope-o"></i> Messages
                    <span class="badge badge-success">42</span>
                </a>
                <div class="dropdown-header text-center">
                    <strong>Settings</strong>
                </div>
                <a class="dropdown-item" href="#">
                    <i class="fa fa-user"></i> Profile</a>
                <a class="dropdown-item" href="#">
                    <i class="fa fa-wrench"></i> Settings</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">
                    <i class="fa fa-shield"></i> Lock Account</a>
                <a href="{{ url('/admin/logout') }}" class="dropdown-item btn btn-default btn-flat"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-lock"></i>Logout
                </a>
                <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</header>

<div class="app-body">
    @include('admin.layouts.sidebar')
    <main class="main">
        @yield('content')
    </main>
</div>
<footer class="app-footer">
    <div>
        <a href="https://infyom.com">InfyOm </a>
        <span>&copy; 2019 InfyOmLabs.</span>
    </div>
    <div class="ml-auto">
        <span>Powered by</span>
        <a href="https://coreui.io">CoreUI</a>
    </div>
</footer>
</body>
<!-- jQuery 3.1.1 -->
<script src="{{ asset('template/admin/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('template/admin/js/popper.min.js') }}"></script>
<script src="{{ asset('template/admin/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/admin/js/moment.min.js') }}"></script>
<script src="{{ asset('template/admin/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('template/admin/js/coreui.min.js') }}"></script>
<script src="{{ asset('template/admin/js/sweetalert2@9.js') }}"></script>
@yield('scripts')

</html>
