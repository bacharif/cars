<li class="nav-item {{ Request::is('admin/cars*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.cars.index') }}">
        <i class="fa fa-car"></i>
        <span>Cars</span>
    </a>
</li>
<li class="nav-item {{ Request::is('admin/sellers*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('admin.sellers.index') }}">
        <i class="fa fa-users"></i>
        <span>Sellers</span>
    </a>
</li>
