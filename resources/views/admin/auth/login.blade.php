<!DOCTYPE html>
<html lang="en">
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Login</title>
    <meta name="description" content="CoreUI Template - InfyOm Laravel Generator">
    <meta name="keyword" content="CoreUI,Bootstrap,Admin,Template,InfyOm,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <!-- Bootstrap-->
    <link rel="stylesheet" href="{{ asset('template/admin/css/bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('template/admin/css/coreui.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('template/admin/css/coreui-icons-free.css') }}">
    <link href="{{ asset('template/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/fontawesome/css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('template/admin/css/simple-line-icons.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('template/admin/css/flag-icon.min.css') }}">
</head>
<body class="app flex-row align-items-center">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-group">
                <div class="card p-4">
                    <div class="card-body">
                        <form method="post" action="{{ url('/admin/login') }}">
                            @csrf
                            <h1>{{ __('Login') }}</h1>
                            <p class="text-muted">{{ __('Sign In to your account') }}</p>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
{{--<i class="fa fa-user"></i>--}}@
                                    </span>
                                </div>
                                <input type="email" class="form-control {{ $errors->has('email')?'is-invalid':'' }}" name="email" value="{{ old('email') }}"
                                       placeholder="Email">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="input-group mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="fa fa-lock"></i>
                                    </span>
                                </div>
                                <input type="password" class="form-control {{ $errors->has('password')?'is-invalid':'' }}" placeholder="Password" name="password">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                       <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <button class="btn btn-primary px-4" type="submit">{{ __('Login') }}</button>
                                </div>
                                <div class="col-6 text-right">
                                    <a class="btn btn-link px-0" href="{{ url('/admin/password/reset') }}">
                                        {{ __('Forgot password') }}?
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- CoreUI and necessary plugins-->
<script src="{{ asset('template/admin/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('template/admin/js/popper.min.js') }}"></script>
<script src="{{ asset('template/admin/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/admin/js/coreui.min.js') }}"></script>
<script src="{{ asset('template/admin/js/perfect-scrollbar.js') }}"></script>
</body>
</html>
