<!DOCTYPE html>
<html lang="en">
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Cars | Register</title>
    <meta name="description" content="CoreUI Template - InfyOm Laravel Generator">
    <meta name="keyword" content="CoreUI,Bootstrap,Admin,Template,InfyOm,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <link rel="stylesheet" href="{{ asset('template/admin/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/admin/css/coreui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/admin/css/coreui-icons-free.css') }}">
    <link rel="stylesheet" href="{{ asset('template/fontawesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/fontawesome/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('template/admin/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('template/admin/css/flag-icon.min.css') }}">

</head>
<body class="app flex-row align-items-center">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card mx-4">
                <div class="card-body p-4">
                    <form method="post" action="{{ url('/admin/register') }}">
                        @csrf
                        <h1>Register</h1>
                        <p class="text-muted">Create your account</p>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="fa fa-user"></i>
                              </span>
                            </div>
                            <input type="text" class="form-control {{ $errors->has('full_name')?'is-invalid':'' }}" name="full_name" value="{{ old('full_name') }}"
                                   placeholder="Full Name">
                            @if ($errors->has('full_name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('full_name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="fa fa-phone"></i>
                              </span>
                            </div>
                            <input type="text" class="form-control {{ $errors->has('phone_number')?'is-invalid':'' }}" name="phone_number" value="{{ old('phone_number') }}"
                                   placeholder="Phone Number">
                            @if ($errors->has('full_name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('full_name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">@</span>
                            </div>
                            <input type="email" class="form-control {{ $errors->has('email')?'is-invalid':'' }}" name="email" value="{{ old('email') }}" placeholder="Email">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="fa fa-lock"></i>
                              </span>
                            </div>
                            <input type="password" class="form-control {{ $errors->has('password')?'is-invalid':''}}" name="password" placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                              <span class="input-group-text">
                                <i class="fa fa-lock"></i>
                              </span>
                            </div>
                            <input type="password" name="password_confirmation" class="form-control"
                                   placeholder="Confirm password">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                  <strong>{{ $errors->first('password_confirmation') }}</strong>
                               </span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                        <a href="{{ url('/admin/login') }}" class="text-center">I already have a membership</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- CoreUI and necessary plugins-->
<script src="{{ asset('template/admin/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('template/admin/js/popper.min.js') }}"></script>
<script src="{{ asset('template/admin/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/admin/js/coreui.min.js') }}"></script>
<script src="{{ asset('template/admin/js/perfect-scrollbar.js') }}"></script>
</body>
</html>
