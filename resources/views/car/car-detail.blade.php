@extends('home_page.home_page_layout')

@section('content')
    <!-- Sub banner start -->
    <div class="sub-banner">
        <div class="container breadcrumb-area">
            <div class="breadcrumb-areas">
                <h1>Détail de la voiture "{{ $car->brand }}"</h1>
                <ul class="breadcrumbs">
                    <li><a href="{{ route('home.page') }}">Home</a></li>
                    <li class="active">Détails de la voiture</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Sub Banner end -->

    <!-- Car details page start -->
    <div class="car-details-page content-area-6">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-xs-12">
                    <div class="car-details-section">
                        {{--<div id="carDetailsSlider" class="carousel car-details-sliders slide mb-40">
                            <!-- main slider carousel items -->
                            @php
                                $slide_number = 0;
                            @endphp
                            <div class="carousel-inner">
                                @foreach($car->carImages as $carImage)
                                    @if($slide_number == 0)
                                        <div class="active item carousel-item" data-slide-number="{{ $slide_number }}">
                                            <img src="{{$carImage->image_url }}" class="img-fluid" alt="slider-car">
                                        </div>
                                    @else
                                        <div class="item carousel-item" data-slide-number="{{ $slide_number }}">
                                            <img src="{{$carImage->image_url }}" class="img-fluid" alt="slider-car">
                                        </div>
                                    @endif
                                    <input type="hidden" value="{{ $slide_number ++ }}">
                                @endforeach
                            </div>

                            <!-- main slider carousel nav controls -->
                            @php
                                $slide_to = 0;
                            @endphp
                            <ul class="carousel-indicators car-properties list-inline nav nav-justified">
                                @foreach($car->carImages as $carImage)
                                    @if($slide_to == 0)
                                        <li class="list-inline-item active">
                                            <a id="{{ 'carousel-selector-'.$slide_to }}" class="selected"
                                               data-slide-to="{{ $slide_to }}"
                                               data-target="#carDetailsSlider">
                                                <img src="{{$carImage->image_url }}" class="img-fluid" alt="small-car">
                                            </a>
                                        </li>
                                    @else
                                        <li class="list-inline-item">
                                            <a id="{{ 'carousel-selector-'.$slide_to }}"
                                               data-slide-to="{{ $slide_to }}"
                                               data-target="#carDetailsSlider">
                                                <img src="{{$carImage->image_url }}" class="img-fluid" alt="small-car">
                                            </a>
                                        </li>
                                    @endif
                                    <input type="hidden" value="{{ $slide_to ++ }}">
                                @endforeach
                            </ul>
                            <div class="heading-car-2">
                                <h3>{{ $car->brand }}</h3>
                                <div class="price-location">
                                    <span class="car-price">{{ $car->price }} F CFA</span>
                                    <span class="rent">{{ $car->status }}</span>
                                    <span class="location">
                                        <i class="flaticon-pin"></i>{{ $car->localisation }},
                                    </span>
                                </div>
                            </div>
                        </div>--}}
                        {{--                    <div id="carDetailsSlider" class="carousel car-details-sliders slide mb-40">--}}
                        <div id="image" class="form-group carousel slide" data-ride="carousel">

                            <!-- Indicators -->
                            <ul class="carousel-indicators">
                                @foreach($car->carImages as $carImage)
                                    <li data-target="#image" data-slide-to="{{ $loop->index }}" class="{{ $loop->index == 0 ? 'active' : '' }}"></li>
                                @endforeach
                            </ul>

                            <!-- main slider carousel items -->
                            <div class="carousel-inner">
                                @foreach($car->carImages as $carImage)
                                    <div class="{{ $loop->index == 0 ? 'carousel-item  active' : 'carousel-item '}}">
                                        <img src="{{ $carImage->image_url }}" alt="Car Image" {{--width="100%"--}}>
                                    </div>
                                @endforeach
                            </div>

                            <!-- Left and right controls -->
                            <a class="carousel-control-prev" href="#image" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a class="carousel-control-next" href="#image" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                            </a>

                            <div class="heading-car-2">
                                <h3>{{ $car->brand }}</h3>
                                <div class="price-location">
                                    <span class="car-price">{{strrev(wordwrap(strrev($car->price), 3, '.', true))}} F CFA</span>
                                    <span class="rent">{{ $car->status }}</span>
                                    <span class="location">
                                    <i class="flaticon-pin"></i>{{ $car->localisation }},
                                </span>
                                </div>
                            </div>
                        </div>

                        <!-- Tabbing box start -->
                        <div class="tabbing tabbing-box mb-40">
                            <ul class="nav nav-tabs" id="carTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active show" id="one-tab" data-toggle="tab" href="#one" role="tab"
                                       aria-controls="one" aria-selected="false">Description</a>
                                </li>
                                {{--<li class="nav-item">
                                    <a class="nav-link" id="6-tab" data-toggle="tab" href="#6" role="tab" aria-controls="6" aria-selected="true">Specifications</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="two" aria-selected="false">Features</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="three" aria-selected="true">Related Car</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="5-tab" data-toggle="tab" href="#5" role="tab" aria-controls="5" aria-selected="true">Location</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="4-tab" data-toggle="tab" href="#4" role="tab" aria-controls="4" aria-selected="true">Video</a>
                                </li>--}}
                            </ul>
                            <div class="tab-content" id="carTabContent">
                                <div class="tab-pane fade active show" id="one" role="tabpanel" aria-labelledby="one-tab">
                                    <div class="car-description mb-50">
                                        <h3 class="heading-2">
                                            Description
                                        </h3>
                                        {{--                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra a. Aliquam pellentesque nibh et nibh feugiat gravida. Maecenas ultricies, diam vitae semper placerat, velit risus accumsan nisl, eget tempor lacus est vel nunc. Proin accumsan elit sed neque euismod fringilla. Curabitur lobortis nunc velit, et fermentum urna dapibus non. Vivamus magna lorem, elementum id gravida ac, laoreet tristique augue. Maecenas dictum lacus eu nunc porttitor, ut hendrerit arcu efficitur.</p>--}}
                                        <p>{{ $car->description }}</p>
                                    </div>
                                </div>

                                <div class="tab-pane fade " id="5" role="tabpanel" aria-labelledby="5-tab">
                                    <div class="location mb-50">
                                        <div class="map">
                                            <h3 class="heading-2">Location</h3>
                                            <div id="map" class="contact-map">{{ $car->location }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-12">
                    <div class="sidebar-right">
                        <!-- Advanced search start -->
                        <div class="widget advanced-search d-none d-xl-block d-lg-block">
                            <h3 class="sidebar-title">{{ __('Characteristics') }}</h3>
                            <ul>
                                <li>
                                    <span>{{__('Reference')}}</span>{{ $car->reference }}
                                </li>
                                <li>
                                    <span>{{__('Make')}}</span>{{ $car->make }}
                                </li>
                                <li>
                                    <span>{{__('Model')}}</span>{{ $car->model }}
                                </li>
                                @if($car->body_style)
                                    <li>
                                        <span>{{__('Body Style')}}</span>{{ $car->body_style }}
                                    </li>
                                @endif
                                <li>
                                    <span>{{__('Year')}}</span>{{ $car->year }}
                                </li>
                                <li>
                                    <span>{{__('Mileage')}}</span> {{ $car->mileage }} mi
                                </li>
                                @if($car->transmission)
                                    <li>
                                        <span>{{__('Transmission')}}</span> {{ $car->transmission }}
                                    </li>
                                @endif
                                <li>
                                    <span>{{__('Engine')}}</span> {{ $car->engine }}
                                </li>
                                <li>
                                    <span>{{__('Location')}}</span> {{ $car->localisation }}
                                </li>
                                <li>
                                    <span>{{__('Fuel Type')}}</span> {{ $car->fuel_type }}
                                </li>
                                <li>
                                    {{--                                <span><i class="fa fa-user"></i>{{__('Contact')}}</span> {{ $car->fuel_type }}--}}
                                    <span>
                                    <img src="{{ asset('template/home_page/img/phone-icon2.png') }}" alt="" style="width: 20px; height: 20px">
                                {{__('Contact')}}
                                </span> {{ $car->seller->phone_number }}
                                </li>
                            </ul>
                            {{--<div class="detail">
                                <h5 class="location">
                                    <a href="{{ route('car.detail', $car->id) }}" style="color: var(--BLUE_COLOR)">{{__('Contact')}}
                                        <i class="fa fa-mobile-phone"></i> {{ $car->seller->phone_number }},
                                    </a>
                                </h5>
                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Car details page end -->
@endsection

@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0N5pbJN10Y1oYFRd0MJ_v2g8W2QT74JE"></script>
<script>
    function LoadMap(propertes) {
        var defaultLat = 40.7110411;
        var defaultLng = -74.0110326;
        var mapOptions = {
            center: new google.maps.LatLng(defaultLat, defaultLng),
            zoom: 15,
            scrollwheel: false,
            styles: [
                {
                    featureType: "administrative",
                    elementType: "labels",
                    stylers: [
                        {visibility: "off"}
                    ]
                },
                {
                    featureType: "water",
                    elementType: "labels",
                    stylers: [
                        {visibility: "off"}
                    ]
                },
                {
                    featureType: 'poi.business',
                    stylers: [{visibility: 'off'}]
                },
                {
                    featureType: 'transit',
                    elementType: 'labels.icon',
                    stylers: [{visibility: 'off'}]
                },
            ]
        };
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
        var infoWindow = new google.maps.InfoWindow();
        var myLatlng = new google.maps.LatLng(40.7110411, -74.0110326);

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map
        });
        (function (marker) {
            google.maps.event.addListener(marker, "click", function (e) {
                infoWindow.setContent("" +
                    "<div class='map-properties contact-map-content'>" +
                    "<div class='map-content'>" +
                    "<p class='address'>20-21 Kathal St. Tampa City, FL</p>" +
                    "<ul class='map-properties-list'> " +
                    "<li><i class='flaticon-phone'></i>  +0477 8556 552</li> " +
                    "<li><i class='flaticon-phone'></i>  info@themevessel.com</li> " +
                    "<li><a href='index.html'><i class='fa fa-globe'></i>  http://www.example.com</li></a> " +
                    "</ul>" +
                    "</div>" +
                    "</div>");
                infoWindow.open(map, marker);
            });
        })(marker);
    }

    LoadMap();
</script>
@endsection
