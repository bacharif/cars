<!DOCTYPE html>
<html lang="zxx">

<!-- Mirrored from storage.googleapis.com/themevessel-products/cmart/car-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Apr 2020 10:20:47 GMT -->
<head>
    <title>CMART - Car Dealer HTML Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/bootstrap-submenu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/magnific-popup.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/leaflet.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/map.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('template/home_page/fonts/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/fonts/flaticon/font/flaticon.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/fonts/linearicons/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/jquery.mCustomScrollbar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/dropzone.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/lightbox.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/jnoty.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/slick.css') }}">

    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/css/style.css') }}">
    <link rel="stylesheet" type="text/css" id="style_sheet"
          href="{{ asset('template/home_page/css/skins/midnight-blue.css') }}">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{ asset('template/home_page/img/favicon.ico') }}" type="image/x-icon">

    <!-- Google fonts -->
    <link href="{{ asset('template/home_page/fonts/google/googleapis.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/home_page/fonts/google/googleapis_raleway.css') }}">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('template/home_page/css/ie10-viewport-bug-workaround.css') }}">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script  src="{{ asset('template/home_page/js/ie8-responsive-file-warning.js') }}"></script><![endif]-->
    <script src="{{ asset('template/home_page/js/ie-emulation-modes-warning.js') }}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script  src="{{ asset('template/home_page/js/html5shiv.min.js') }}"></script>
    <script  src="{{ asset('template/home_page/js/respond.min.js') }}"></script>
    <![endif]-->
</head>
<body>
<div class="page_loader"></div>

<!-- Main header start -->
<header class="main-header sticky-header header-with-top">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand company-logo" href="{{ route('home.page') }}">
                <img src="{{ asset('template/home_page/img/logos/black-logo.png') }}" alt="logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
                    aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fa fa-bars"></span>
            </button>
            <div class="navbar-collapse collapse w-100" id="navbar">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            Home
                        </a>
                        {{-- <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                             <li><a class="dropdown-item" href="index.html">Home 1</a></li>
                             <li><a class="dropdown-item" href="index-2.html">Home 2</a></li>
                             <li><a class="dropdown-item" href="index-3.html">Home 3</a></li>
                             <li><a class="dropdown-item" href="index-4.html">Home 4</a></li>
                             <li><a class="dropdown-item" href="index-5.html">Home 5</a></li>
                             <li><a class="dropdown-item" href="index-6.html">Home 6</a></li>
                         </ul>--}}
                    </li>
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            Car Listing
                        </a>
                        {{--<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">List
                                    Layout</a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="car-list-rightside.html">List Right Sidebar</a>
                                    </li>
                                    <li><a class="dropdown-item" href="car-list-leftsidebar.html">List Left Sidebar</a>
                                    </li>
                                    <li><a class="dropdown-item" href="car-list-fullWidth.html">List FullWidth</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Grid
                                    Layout</a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="car-grid-rightside.html">Grid Right Sidebar</a>
                                    </li>
                                    <li><a class="dropdown-item" href="car-grid-leftside.html">Grid Left Sidebar</a>
                                    </li>
                                    <li><a class="dropdown-item" href="car-grid-fullWidth.html">Grid FullWidth</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Car
                                    Details</a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="../car/car-details.html">Car Details 1</a></li>
                                    <li><a class="dropdown-item" href="car-details-2.html">Car Details 2</a></li>
                                    <li><a class="dropdown-item" href="car-details-3.html">Car Details 3</a></li>
                                </ul>
                            </li>
                        </ul>--}}
                    </li>
                    <li class="nav-item dropdown megamenu-li">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">Pages</a>
                        {{--<div class="dropdown-menu megamenu" aria-labelledby="dropdown01">
                            <div class="megamenu-area">
                                <div class="row">
                                    <div class="col-sm-6 col-md-4 col-lg-4">
                                        <div class="megamenu-section">
                                            <h6 class="megamenu-title">Pages</h6>
                                            <a class="dropdown-item" href="about.html">About Us</a>
                                            <a class="dropdown-item" href="services.html">Services</a>
                                            <a class="dropdown-item" href="agent.html">Agent</a>
                                            <a class="dropdown-item" href="car-comparison.html">Car Compare</a>
                                            <a class="dropdown-item" href="car-reviews.html">Car Reviews</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-lg-4">
                                        <div class="megamenu-section">
                                            <h6 class="megamenu-title">Pages</h6>
                                            <a class="dropdown-item" href="gallery.html">Gallery</a>
                                            <a class="dropdown-item" href="typography.html">Typography</a>
                                            <a class="dropdown-item" href="pricing-tables.html">Pricing Tables</a>
                                            <a class="dropdown-item" href="elements.html">Elements</a>
                                            <a class="dropdown-item" href="faq.html">Faq</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-lg-4">
                                        <div class="megamenu-section">
                                            <h6 class="megamenu-title">Pages</h6>
                                            <a class="dropdown-item" href="icon.html">Icons</a>
                                            <a class="dropdown-item" href="coming-soon.html">Coming Soon</a>
                                            <a class="dropdown-item" href="404.html">Error Page</a>
                                            <a class="dropdown-item" href="login.html">login</a>
                                            <a class="dropdown-item" href="signup.html">Register</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--}}
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink6" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            Blog
                        </a>
                        {{-- <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                             <li><a class="dropdown-item" href="blog-columns-2col.html">Blog Columns 2</a></li>
                             <li><a class="dropdown-item" href="blog-columns-3col.html">Blog Columns 3</a></li>
                             <li><a class="dropdown-item" href="blog-right-sidebar.html">Blog Right Sidebar</a></li>
                             <li><a class="dropdown-item" href="blog-details.html">Blog Details</a></li>
                         </ul>--}}
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink7" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            Shop
                        </a>
                        {{--<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item" href="shop-list.html">Shop List</a></li>
                            <li><a class="dropdown-item" href="shop-cart.html">Shop Cart</a></li>
                            <li><a class="dropdown-item" href="shop-checkout.html">Shop Checkout</a></li>
                            <li><a class="dropdown-item" href="shop-details.html">Shop Details</a></li>
                        </ul>--}}
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#">Contact</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
<!-- Main header end -->

<!-- Sub banner start -->
<div class="sub-banner">
    <div class="container breadcrumb-area">
        <div class="breadcrumb-areas">
            <h1>Car Details</h1>
            <ul class="breadcrumbs">
                <li><a href="{{ route('home.page') }}">Home</a></li>
                <li class="active">Car Details</li>
            </ul>
        </div>
    </div>
</div>
<!-- Sub Banner end -->

<!-- Car details page start -->
<div class="car-details-page content-area-6">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 col-xs-12">
                <div class="car-details-section">
                    {{--<div id="carDetailsSlider" class="carousel car-details-sliders slide mb-40">
                        <!-- main slider carousel items -->
                        @php
                            $slide_number = 0;
                        @endphp
                        <div class="carousel-inner">
                            @foreach($car->carImages as $carImage)
                                @if($slide_number == 0)
                                    <div class="active item carousel-item" data-slide-number="{{ $slide_number }}">
                                        <img src="{{$carImage->image_url }}" class="img-fluid" alt="slider-car">
                                    </div>
                                @else
                                    <div class="item carousel-item" data-slide-number="{{ $slide_number }}">
                                        <img src="{{$carImage->image_url }}" class="img-fluid" alt="slider-car">
                                    </div>
                                @endif
                                <input type="hidden" value="{{ $slide_number ++ }}">
                            @endforeach
                        </div>

                        <!-- main slider carousel nav controls -->
                        @php
                            $slide_to = 0;
                        @endphp
                        <ul class="carousel-indicators car-properties list-inline nav nav-justified">
                            @foreach($car->carImages as $carImage)
                                @if($slide_to == 0)
                                    <li class="list-inline-item active">
                                        <a id="{{ 'carousel-selector-'.$slide_to }}" class="selected"
                                           data-slide-to="{{ $slide_to }}"
                                           data-target="#carDetailsSlider">
                                            <img src="{{$carImage->image_url }}" class="img-fluid" alt="small-car">
                                        </a>
                                    </li>
                                @else
                                    <li class="list-inline-item">
                                        <a id="{{ 'carousel-selector-'.$slide_to }}"
                                           data-slide-to="{{ $slide_to }}"
                                           data-target="#carDetailsSlider">
                                            <img src="{{$carImage->image_url }}" class="img-fluid" alt="small-car">
                                        </a>
                                    </li>
                                @endif
                                <input type="hidden" value="{{ $slide_to ++ }}">
                            @endforeach
                        </ul>
                        <div class="heading-car-2">
                            <h3>{{ $car->brand }}</h3>
                            <div class="price-location">
                                <span class="car-price">{{ $car->price }} F CFA</span>
                                <span class="rent">{{ $car->status }}</span>
                                <span class="location">
                                    <i class="flaticon-pin"></i>{{ $car->localisation }},
                                </span>
                            </div>
                        </div>
                    </div>--}}
                    {{--                    <div id="carDetailsSlider" class="carousel car-details-sliders slide mb-40">--}}
                    <div id="image" class="form-group carousel slide" data-ride="carousel">

                        <!-- Indicators -->
                        <ul class="carousel-indicators">
                            @foreach($car->carImages as $carImage)
                                <li data-target="#image" data-slide-to="{{ $loop->index }}" class="{{ $loop->index == 0 ? 'active' : '' }}"></li>
                            @endforeach
                        </ul>

                        <!-- main slider carousel items -->
                        <div class="carousel-inner">
                            @foreach($car->carImages as $carImage)
                                <div class="{{ $loop->index == 0 ? 'carousel-item  active' : 'carousel-item '}}">
                                    <img src="{{ $carImage->image_url }}" alt="Car Image" {{--width="100%"--}}>
                                </div>
                            @endforeach
                        </div>

                        <!-- Left and right controls -->
                        <a class="carousel-control-prev" href="#image" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a class="carousel-control-next" href="#image" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a>

                        <div class="heading-car-2">
                            <h3>{{ $car->brand }}</h3>
                            <div class="price-location">
                                <span class="car-price">{{strrev(wordwrap(strrev($car->price), 3, '.', true))}} F CFA</span>
                                <span class="rent">{{ $car->status }}</span>
                                <span class="location">
                                    <i class="flaticon-pin"></i>{{ $car->localisation }},
                                </span>
                            </div>
                        </div>
                    </div>

                    <!-- Tabbing box start -->
                    <div class="tabbing tabbing-box mb-40">
                        <ul class="nav nav-tabs" id="carTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active show" id="one-tab" data-toggle="tab" href="#one" role="tab"
                                   aria-controls="one" aria-selected="false">Description</a>
                            </li>
                            {{--<li class="nav-item">
                                <a class="nav-link" id="6-tab" data-toggle="tab" href="#6" role="tab" aria-controls="6" aria-selected="true">Specifications</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="two" aria-selected="false">Features</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="three" aria-selected="true">Related Car</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="5-tab" data-toggle="tab" href="#5" role="tab" aria-controls="5" aria-selected="true">Location</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="4-tab" data-toggle="tab" href="#4" role="tab" aria-controls="4" aria-selected="true">Video</a>
                            </li>--}}
                        </ul>
                        <div class="tab-content" id="carTabContent">
                            <div class="tab-pane fade active show" id="one" role="tabpanel" aria-labelledby="one-tab">
                                <div class="car-description mb-50">
                                    <h3 class="heading-2">
                                        Description
                                    </h3>
                                    {{--                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra a. Aliquam pellentesque nibh et nibh feugiat gravida. Maecenas ultricies, diam vitae semper placerat, velit risus accumsan nisl, eget tempor lacus est vel nunc. Proin accumsan elit sed neque euismod fringilla. Curabitur lobortis nunc velit, et fermentum urna dapibus non. Vivamus magna lorem, elementum id gravida ac, laoreet tristique augue. Maecenas dictum lacus eu nunc porttitor, ut hendrerit arcu efficitur.</p>--}}
                                    <p>{{ $car->description }}</p>
                                </div>
                            </div>

                            <div class="tab-pane fade " id="5" role="tabpanel" aria-labelledby="5-tab">
                                <div class="location mb-50">
                                    <div class="map">
                                        <h3 class="heading-2">Location</h3>
                                        <div id="map" class="contact-map">{{ $car->location }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-12">
                <div class="sidebar-right">
                    <!-- Advanced search start -->
                    <div class="widget advanced-search d-none d-xl-block d-lg-block">
                        <h3 class="sidebar-title">{{ __('Characteristics') }}</h3>
                        <ul>
                            <li>
                                <span>{{__('Make')}}</span>{{ $car->make }}
                            </li>
                            <li>
                                <span>{{__('Model')}}</span>{{ $car->model }}
                            </li>
                            @if($car->body_style)
                                <li>
                                    <span>{{__('Body Style')}}</span>{{ $car->body_style }}
                                </li>
                            @endif
                            <li>
                                <span>{{__('Year')}}</span>{{ $car->year }}
                            </li>
                            <li>
                                <span>{{__('Mileage')}}</span> {{ $car->mileage }} mi
                            </li>
                            @if($car->transmission)
                                <li>
                                    <span>{{__('Transmission')}}</span> {{ $car->transmission }}
                                </li>
                            @endif
                            <li>
                                <span>{{__('Engine')}}</span> {{ $car->engine }}
                            </li>
                            <li>
                                <span>{{__('Location')}}</span> {{ $car->localisation }}
                            </li>
                            <li>
                                <span>{{__('Fuel Type')}}</span> {{ $car->fuel_type }}
                            </li>
                            <li>
                                {{--                                <span><i class="fa fa-user"></i>{{__('Contact')}}</span> {{ $car->fuel_type }}--}}
                                <span>
                                    <img src="{{ asset('template/home_page/img/phone-icon2.png') }}" alt="" style="width: 20px; height: 20px">
                                {{__('Contact')}}
                                </span> {{ $car->seller->phone_number }}
                            </li>
                        </ul>
                        {{--<div class="detail">
                            <h5 class="location">
                                <a href="{{ route('car.detail', $car->id) }}" style="color: var(--BLUE_COLOR)">{{__('Contact')}}
                                    <i class="fa fa-mobile-phone"></i> {{ $car->seller->phone_number }},
                                </a>
                            </h5>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Car details page end -->

<!-- Footer start -->
<footer class="footer">
    <div class="container footer-inner">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="footer-item clearfix">
                    <img src="{{ asset('template/home_page/img/logos/logo.png') }}" alt="logo" class="f-logo">
                    <ul class="contact-info">
                        <li>
                            <i class="flaticon-pin"></i>20/F Green Road, Dhanmondi, Dhaka
                        </li>
                        <li>
                            <i class="flaticon-mail"></i><a
                                href="mailto:sales@hotelempire.com">info@themevessel.com</a>
                        </li>
                        <li>
                            <i class="flaticon-phone"></i><a href="tel:+55-417-634-7071">+0477 85X6 552</a>
                        </li>
                        <li>
                            <i class="flaticon-fax"></i>+0477 85X6 552
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="social-list-2">
                        <ul>
                            <li><a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" class="google-bg"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" class="linkedin-bg"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6">
                <div class="footer-item">
                    <h4>
                        Useful Links
                    </h4>
                    <ul class="links">
                        <li>
                            <a href="#">Home</a>
                        </li>
                        <li>
                            <a href="about.html">About Us</a>
                        </li>
                        <li>
                            <a href="services.html">Services</a>
                        </li>
                        <li>
                            <a href="faq.html">Faq</a>
                        </li>
                        <li>
                            <a href="contact.html">Contact Us</a>
                        </li>
                        <li>
                            <a href="car-comparison.html">Car Compare</a>
                        </li>
                        <li>
                            <a href="car-reviews.html">Car Reviews</a>
                        </li>
                        <li>
                            <a href="elements.html">Elements</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <div class="recent-posts footer-item">
                    <h4>Recent Posts</h4>
                    <div class="media mb-4">
                        <a class="pr-3" href="car-details.html">
                            <img class="media-object" src="{{ asset('template/home_page/img/car/small-car-3.jpg') }}" alt="small-car">
                        </a>
                        <div class="media-body align-self-center">
                            <h5>
                                <a href="car-details.html">Bentley Continental GT</a>
                            </h5>
                            <div class="listing-post-meta">
                                $345,00 | <a href="#"><i class="fa fa-calendar"></i> Jan 12, 2019</a>
                            </div>
                        </div>
                    </div>
                    <div class="media mb-4">
                        <a class="pr-3" href="car-details.html">
                            <img class="media-object" src="{{ asset('template/home_page/img/car/small-car-1.jpg') }}" alt="small-car">
                        </a>
                        <div class="media-body align-self-center">
                            <h5>
                                <a href="car-details.html">Fiat Punto Red</a>
                            </h5>
                            <div class="listing-post-meta">
                                $745,00 | <a href="#"><i class="fa fa-calendar"></i> Feb 26, 2019</a>
                            </div>
                        </div>
                    </div>
                    <div class="media">
                        <a class="pr-3" href="car-details.html">
                            <img class="media-object" src="{{ asset('template/home_page/img/car/small-car-2.jpg') }}" alt="small-car">
                        </a>
                        <div class="media-body align-self-center">
                            <h5>
                                <a href="car-details.html">Nissan Micra Gen5</a>
                            </h5>
                            <div class="listing-post-meta">
                                $745,00 | <a href="#"><i class="fa fa-calendar"></i> Feb 14, 2019</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="footer-item clearfix">
                    <h4>Subscribe</h4>
                    <div class="Subscribe-box">
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                            mollit.</p>
                        <form class="form-inline" action="#" method="GET">
                            <input type="text" class="form-control mb-sm-0" id="inlineFormInputName3"
                                   placeholder="Email Address">
                            <button type="submit" class="btn"><i class="fa fa-paper-plane"></i></button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xl-12 col-lg-12">
                <p class="copy sub-footer">© 2019 <a href="#">Theme Vessel.</a> Trademarks and brands are the
                    property of their respective owners.</p>
            </div>
        </div>
    </div>
</footer>
<!-- Footer end -->

<!-- Full Page Search -->
<div id="full-page-search">
    <button type="button" class="close">×</button>
    <form action="http://storage.googleapis.com/themevessel-products/cmart/index.html#">
        <input type="search" value="" placeholder="type keyword(s) here"/>
        <button type="submit" class="btn btn-sm button-theme">Search</button>
    </form>
</div>

<!-- Car Overview Modal -->
<div class="car-model-2">
    <div class="modal fade" id="carOverviewModal" tabindex="-1" role="dialog"
         aria-labelledby="carOverviewModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="carOverviewModalLabel">
                        Find Your Dream Car
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row modal-raw">
                        <div class="col-lg-5 modal-left">
                            <div class="modal-left-content">
                                <div class="bs-example" data-example-id="carousel-with-captions">
                                    <div class="carousel slide" id="properties-carousel" data-ride="carousel">
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img src="{{ asset('template/home_page/img/car/car-5.jpg') }}" alt="best-car" class="img-fluid">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="description">
                                    <h3>Description</h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry. Lorem Ipsum has been the industry's standard dummy text ever
                                        since the 1500s, when an unknown printer took a galley of type and
                                        scrambled.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7 modal-right">
                            <div class="modal-right-content">
                                <section>
                                    <h3>Features</h3>
                                    <div class="features">
                                        <ul class="bullets">
                                            <li>Cruise Control</li>
                                            <li>Airbags</li>
                                            <li>Air Conditioning</li>
                                            <li>Alarm System</li>
                                            <li>Audio Interface</li>
                                            <li>CDR Audio</li>
                                            <li>Seat Heating</li>
                                            <li>ParkAssist</li>
                                        </ul>
                                    </div>
                                </section>
                                <section>
                                    <h3>Overview</h3>
                                    <dl>
                                        <dt>Model</dt>
                                        <dd>Audi</dd>
                                        <dt>Condition</dt>
                                        <dd>Brand New</dd>
                                        <dt>Year</dt>
                                        <dd>2020</dd>
                                        <dt>Price</dt>
                                        <dd>$178,000</dd>
                                    </dl>
                                </section>
                                <div class="price-box-2"><sup>$</sup>780<span>/month</span></div>
                                <div class="dd">
                                    <div class="float-left">
                                        <div class="ratings-2">
                                            <span class="ratings-box">4.5/5</span>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <span>( 7 Reviews )</span>
                                        </div>
                                    </div>
                                    <div class="float-right">
                                        <a href="car-details.html" class="btn btn-md btn-round btn-theme">Show
                                            Detail</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="{{ asset('template/home_page/js/jquery-2.2.0.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/popper.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/bootstrap-submenu.js') }}"></script>
<script src="{{ asset('template/home_page/js/rangeslider.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.mb.YTPlayer.js') }}"></script>
<script src="{{ asset('template/home_page/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.easing.1.3.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.scrollUp.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/leaflet.js') }}"></script>
<script src="{{ asset('template/home_page/js/leaflet-providers.js') }}"></script>
<script src="{{ asset('template/home_page/js/leaflet.markercluster.js') }}"></script>
<script src="{{ asset('template/home_page/js/dropzone.js') }}"></script>
<script src="{{ asset('template/home_page/js/slick.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.filterizr.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.countdown.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.mousewheel.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/lightgallery-all.js') }}"></script>
<script src="{{ asset('template/home_page/js/jnoty.js') }}"></script>
<script src="{{ asset('template/home_page/js/maps.js') }}"></script>
<script src="{{ asset('template/home_page/js/app.js') }}"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="{{ asset('template/home_page/js/ie10-viewport-bug-workaround.js') }}"></script>
<!-- Custom javascript -->
<script src="{{ asset('template/home_page/js/ie10-viewport-bug-workaround.js') }}"></script>


{{--
<script src="{{ asset('template/home_page/js/jquery-2.2.0.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/popper.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/bootstrap-submenu.js') }}"></script>
<script src="{{ asset('template/home_page/js/rangeslider.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.mb.YTPlayer.js') }}"></script>
<script src="{{ asset('template/home_page/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.easing.1.3.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.scrollUp.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/leaflet.js') }}"></script>
<script src="{{ asset('template/home_page/js/leaflet-providers.js') }}"></script>
<script src="{{ asset('template/home_page/js/leaflet.markercluster.js') }}"></script>
<script src="{{ asset('template/home_page/js/dropzone.js') }}"></script>
<script src="{{ asset('template/home_page/js/slick.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.filterizr.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.countdown.js') }}"></script>
<script src="{{ asset('template/home_page/js/jquery.mousewheel.min.js') }}"></script>
<script src="{{ asset('template/home_page/js/lightgallery-all.js') }}"></script>
<script src="{{ asset('template/home_page/js/jnoty.js') }}"></script>
<script src="{{ asset('template/home_page/js/maps.js') }}"></script>
<script src="{{ asset('template/home_page/js/app.js') }}"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="{{ asset('template/home_page/js/ie10-viewport-bug-workaround.js') }}"></script>
<!-- Custom javascript -->
<script src="{{ asset('template/home_page/js/ie10-viewport-bug-workaround.js') }}"></script>
--}}

{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0N5pbJN10Y1oYFRd0MJ_v2g8W2QT74JE"></script>--}}
{{--<script>
    function LoadMap(propertes) {
        var defaultLat = 40.7110411;
        var defaultLng = -74.0110326;
        var mapOptions = {
            center: new google.maps.LatLng(defaultLat, defaultLng),
            zoom: 15,
            scrollwheel: false,
            styles: [
                {
                    featureType: "administrative",
                    elementType: "labels",
                    stylers: [
                        {visibility: "off"}
                    ]
                },
                {
                    featureType: "water",
                    elementType: "labels",
                    stylers: [
                        {visibility: "off"}
                    ]
                },
                {
                    featureType: 'poi.business',
                    stylers: [{visibility: 'off'}]
                },
                {
                    featureType: 'transit',
                    elementType: 'labels.icon',
                    stylers: [{visibility: 'off'}]
                },
            ]
        };
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
        var infoWindow = new google.maps.InfoWindow();
        var myLatlng = new google.maps.LatLng(40.7110411, -74.0110326);

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map
        });
        (function (marker) {
            google.maps.event.addListener(marker, "click", function (e) {
                infoWindow.setContent("" +
                    "<div class='map-properties contact-map-content'>" +
                    "<div class='map-content'>" +
                    "<p class='address'>20-21 Kathal St. Tampa City, FL</p>" +
                    "<ul class='map-properties-list'> " +
                    "<li><i class='flaticon-phone'></i>  +0477 8556 552</li> " +
                    "<li><i class='flaticon-phone'></i>  info@themevessel.com</li> " +
                    "<li><a href='index.html'><i class='fa fa-globe'></i>  http://www.example.com</li></a> " +
                    "</ul>" +
                    "</div>" +
                    "</div>");
                infoWindow.open(map, marker);
            });
        })(marker);
    }

    LoadMap();
</script>--}}

</body>

<!-- Mirrored from storage.googleapis.com/themevessel-products/cmart/car-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Apr 2020 10:20:47 GMT -->
</html>
