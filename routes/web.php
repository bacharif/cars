<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'CarController@index')->name('home.page');

Route::name('car.')->group(function () {
    Route::get('car/detail/{id}', 'CarController@detail')->name('detail');
});


Route::name('admin.')->prefix('admin')->namespace('Admin')->group( function (){

    Auth::routes(['verify' => true]);

    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('cars', 'CarController');
    Route::post('/car/position/update', 'CarController@update_position');
    Route::get('car/restore/{id}', 'CarController@restore')->name('car.restore');
    Route::get('cars/restore_all', 'CarController@restore_all')->name('cars.restore_all');
    Route::delete('car/delete_image', 'CarController@delete_image')->name('car.delete.image');
});


Route::group(['prefix' => 'admin'], function () {
    Route::resource('sellers', 'Admin\SellerController', ["as" => 'admin']);
});
