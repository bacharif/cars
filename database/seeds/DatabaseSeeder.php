<?php

use App\Models\Admin;
use App\Models\Seller;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (Admin::whereEmail("admin@228cars.fr")->doesntExist()) {
            Admin::create([
                'full_name' => 'Admin Cars',
                'phone_number' => '92931341',
                'email' => 'admin@228cars.fr',
                'password' => Hash::make('password')
            ]);
        }

        if (Seller::whereEmail("seller@228cars.fr")->doesntExist()) {
            Seller::create([
                'full_name' => 'Seller 228Cars',
                'phone_number' => '92931341',
                'email' => 'seller@228cars.fr',
            ]);
        }

    }
}
