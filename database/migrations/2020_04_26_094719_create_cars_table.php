<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference')->unique()->nullable();
            $table->bigInteger('position')->unsigned()->default(0);
            $table->string('brand');
            $table->string('make');
            $table->string('model');
            $table->string('fuel_type');
            $table->string('engine');
            $table->string('mileage');
            $table->string('transmission')->nullable();
            $table->integer('gears_num')->nullable();
            $table->string('body_style')->nullable();
            $table->string('type');
            $table->string('state');
            $table->integer('year');
            $table->integer('price');
            $table->string('localisation');
            $table->string('status');
            $table->text('description');
            $table->unsignedBigInteger('seller_id')->nullable();
            $table->boolean('visibility');
            $table->foreign('seller_id')->references('id')->on('sellers')->onDelete('set null');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
