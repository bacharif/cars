<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Seller;
use Faker\Generator as Faker;

$factory->define(Seller::class, function (Faker $faker) {

    return [
        'full_name' => $faker->word,
        'phone_number' => $faker->word,
        'email' => $faker->word,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
