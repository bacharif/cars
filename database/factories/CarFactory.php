<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Car;
use Faker\Generator as Faker;

$factory->define(Car::class, function (Faker $faker) {

    return [
        'brand' => $faker->word,
        'make' => $faker->word,
        'model' => $faker->word,
        'fuel_type' => $faker->word,
        'engine' => $faker->word,
        'mileage' => $faker->word,
        'transmission' => $faker->word,
        'gears_num' => $faker->word,
        'body_style' => $faker->word,
        'type' => $faker->word,
        'state' => $faker->word,
        'year' => $faker->randomDigitNotNull,
        'price' => $faker->randomDigitNotNull,
        'localisation' => $faker->word,
        'status' => $faker->word,
        'description' => $faker->text,
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
