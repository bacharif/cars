<?php

namespace App\Http\Controllers;

use App\Models\Car;
use Illuminate\Http\Request;

class CarController extends Controller
{
    public function index()
    {
        $cars_to_select = Car::whereVisibility(1)->get();

        $brands = $cars_to_select->map(function ($item){ return $item->brand; })->unique();

        $makes = $cars_to_select->map(function ($item){ return $item->make; })->unique();

        $locations = $cars_to_select->map(function ($item){ return $item->localisation; })->unique();

        $years = $cars_to_select->map(function ($item){ return $item->year; })->unique();

        $types = $cars_to_select->map(function ($item){ return $item->type; })->unique();

        $last_pub = Car::latest()->take(5)->get();

        if (request()->has('select-brand')){

            $brand = request()->get('select-brand');
            $make = request()->get('select-make');
            $location = request()->get('select-location');
            $year = request()->get('select-year');
            $type = request()->get('select-type');

            $cars = Car::with('carImages')
                ->when($brand, function ($query, $brand){
                    return $query->where('brand', 'LIKE', '%'. $brand . '%');
                })
                ->when($make, function ($query, $make){
                    return $query->where('make', 'LIKE', '%'. $make . '%');
                })
                ->when($location, function ($query, $location){
                    return $query->where('localisation', 'LIKE', '%'. $location . '%');
                })
                ->when($year, function ($query, $year){
                    return $query->where('year', 'LIKE', '%'. $year . '%');
                })
                ->when($type, function ($query, $type){
                    return $query->where('type', 'LIKE', '%'. $type . '%');
                })
                ->orderBy('position')->paginate(4);
        }
        else {
            $cars = Car::whereVisibility(1)->with('carImages')->orderBy('position')->paginate(4);
            $last_pub = Car::whereVisibility(1)->latest()->take(5)->with('carImages')->orderBy('position')->paginate(4);
        }

        return view('home_page.index', compact('cars', 'last_pub', 'cars_to_select', 'brands', 'makes', 'locations', 'years', 'types'));
    }

    public function detail($id)
    {
        $car = Car::with('carImages')->find($id);

        return view('car.car-detail', compact('car'));
    }
}
