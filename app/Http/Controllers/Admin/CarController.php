<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateCarRequest;
use App\Http\Requests\Admin\UpdateCarRequest;
use App\Models\Admin;
use App\Models\Car;
use App\Models\CarImage;
use App\Models\Seller;
use App\Repositories\Admin\CarRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Laracasts\Flash\Flash;
use Response;
use function foo\func;

class CarController extends AppBaseController
{
    /** @var  CarRepository */
    private $carRepository;

    public function __construct(CarRepository $carRepo)
    {
        $this->carRepository = $carRepo;
        $this->middleware('auth:' . Admin::GUARD);
    }

    /**
     * Display a listing of the Car.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $cars = $this->carRepository->all()->sortBy('position');
        $trashed_cars = Car::onlyTrashed()->get();

        return view('admin.cars.index')->with(['cars' => $cars, 'trashed_cars' => $trashed_cars]);
    }

    /**
     * Update position of a car
     * @param Request $request
     */
    public function update_position(Request $request)
    {
        $cars = Car::all();
        foreach ($cars as $car){
            $car->timestamps = false;
            $id = $car->id;

            foreach ($request->positions as $position){
                if ($position['id'] == $id)
                    $car->update(['position' => $position['position']]);
            }
        }
    }
    /**
     * Show the form for creating a new Car.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $current_year = (int) date('Y');
        $years = collect(range($current_year - 30, $current_year))->mapWithKeys(function ($item){
            return [$item => $item];
        });

        $sellers = Seller::all()->mapWithKeys(function ($item){
           return [$item->id => $item->full_name . ' (' . $item->phone_number . ')'];
        });

        return view('admin.cars.create', compact('years', 'sellers'));
    }

    /**
     * Store a newly created Car in storage.
     *
     * @param CreateCarRequest $request
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function store(CreateCarRequest $request)
    {
        $input = $request->all();
        $input += ['position' => Car::all()->count() + 1];

        $car = $this->carRepository->create($input);
        $car->update(['reference' => $this->generate_ref($car->id)]);

        $this->insert_images($request, $car);

        Flash::success('Car saved successfully.');

        return redirect(route('admin.cars.index'));
    }

    /**
     * Display the specified Car.
     *
     * @param int $id
     *
     * @return \Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $car = $this->carRepository->find($id);

        if (empty($car)) {
            Flash::error('Car not found');

            return redirect(route('admin.cars.index'));
        }

        return view('admin.cars.show')->with('car', $car);
    }

    /**
     * Show the form for editing the specified Car.
     *
     * @param int $id
     *
     * @return \Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $car = $this->carRepository->find($id);

        if (empty($car)) {
            Flash::error('Car not found');

            return redirect(route('admin.cars.index'));
        }
        $current_year = (int) date('Y');
        $years = collect(range($current_year - 30, $current_year))->mapWithKeys(function ($item){
            return [$item => $item];
        });

        $sellers = Seller::all()->mapWithKeys(function ($item){
            return [$item->id => $item->full_name . ' (' . $item->phone_number . ')'];
        });

        return view('admin.cars.edit')->with(['car' => $car, 'edit' => true, 'years' => $years, 'sellers' => $sellers]);
    }

    /**
     * Update the specified Car in storage.
     *
     * @param int $id
     * @param UpdateCarRequest $request
     *
     * @return \Illuminate\Routing\Redirector
     */
    public function update($id, UpdateCarRequest $request)
    {
        $car = $this->carRepository->find($id);

        if (empty($car)) {
            Flash::error('Car not found');

            return redirect(route('admin.cars.index'));
        }

        $car = $this->carRepository->update($request->all(), $id);
        $this->insert_images($request, $car);

        Flash::success('Car updated successfully.');

        return redirect(route('admin.cars.index'));
    }

    /**
     * Remove the specified Car from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Routing\Redirector
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        $car = $this->carRepository->find($id);

        if (empty($car)) {
            Flash::error('Car not found');

            return redirect(route('admin.cars.index'));
        }

        $this->carRepository->delete($id);

        Flash::success('Car deleted successfully.');

        return redirect(route('admin.cars.index'));
    }

    /**
     * @param Request $request
     * @param Car|Model $car
     */
    public function insert_images(Request $request, Car $car)
    {
        if ($request->hasFile('images')){
            foreach ($request->file('images') as $image) {
                if (Str::startsWith($image->getClientMimeType(), 'image/')) {
                    $image_name = (string)Str::uuid() . '.' . $image->getClientOriginalExtension();
                    $image_resized = Image::make($image)->resize(750, 500)->encode($image->getClientOriginalExtension());
                    Storage::put('public/cars/' . $car->id . '/' . $image_name, $image_resized);

                    CarImage::create([
                        'name' => $image_name,
                        'car_id' => $car->id,
                    ]);
                }
            }
        }
    }

    public function restore($id)
    {
        Car::onlyTrashed()->where('id', $id)->restore();

        Flash::success('Car restored successfully.');
        return redirect(route('admin.cars.index'));
    }

    public function restore_all()
    {
        Car::onlyTrashed()->restore();

        Flash::success('All Cars restored successfully.');
        return redirect(route('admin.cars.index'));
    }

    public function delete_image(Request $request)
    {
        if ($request->id) {
            $image = CarImage::findOrFail($request->id);
            $image_name = $image->name;
            $car_id = $image->car->id;
            if ($image->delete()) {
                Storage::delete('public/cars/' . $car_id . '/' . $image_name);
            }
        }
    }

    function generate_ref($id)
    {
        $register = (string)$id;
        for ($i = strlen($register); $i < 5; $i++) {
            $register = "0" . $register;
        }
        return "CAR" . $register;
    }

}
