<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\SellerDataTable;
use App\Models\Admin;
use App\Http\Requests\Admin\CreateSellerRequest;
use App\Http\Requests\Admin\UpdateSellerRequest;
use App\Repositories\Admin\SellerRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class SellerController extends AppBaseController
{
    /** @var  SellerRepository */
    private $sellerRepository;

    public function __construct(SellerRepository $sellerRepo)
    {
        $this->middleware('auth:' . Admin::GUARD);
        $this->sellerRepository = $sellerRepo;
    }

    /**
     * Display a listing of the Seller.
     *
     * @param SellerDataTable $sellerDataTable
     * @return Response
     */
    public function index(SellerDataTable $sellerDataTable)
    {
        return $sellerDataTable->render('admin.sellers.index');
    }

    /**
     * Show the form for creating a new Seller.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.sellers.create');
    }

    /**
     * Store a newly created Seller in storage.
     *
     * @param CreateSellerRequest $request
     *
     * @return Response
     */
    public function store(CreateSellerRequest $request)
    {
        $input = $request->all();

        $seller = $this->sellerRepository->create($input);

        Flash::success('Seller saved successfully.');

        return redirect(route('admin.sellers.index'));
    }

    /**
     * Display the specified Seller.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            Flash::error('Seller not found');

            return redirect(route('admin.sellers.index'));
        }

        return view('admin.sellers.show')->with('seller', $seller);
    }

    /**
     * Show the form for editing the specified Seller.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            Flash::error('Seller not found');

            return redirect(route('admin.sellers.index'));
        }

        return view('admin.sellers.edit')->with('seller', $seller);
    }

    /**
     * Update the specified Seller in storage.
     *
     * @param  int              $id
     * @param UpdateSellerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSellerRequest $request)
    {
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            Flash::error('Seller not found');

            return redirect(route('admin.sellers.index'));
        }

        $seller = $this->sellerRepository->update($request->all(), $id);

        Flash::success('Seller updated successfully.');

        return redirect(route('admin.sellers.index'));
    }

    /**
     * Remove the specified Seller from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            Flash::error('Seller not found');

            return redirect(route('admin.sellers.index'));
        }

        $this->sellerRepository->delete($id);

        Flash::success('Seller deleted successfully.');

        return redirect(route('admin.sellers.index'));
    }
}
