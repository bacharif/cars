<?php

namespace App\Repositories\Admin;

use App\Models\Car;
use App\Repositories\BaseRepository;

/**
 * Class CarRepository
 * @package App\Repositories\Admin
 * @version April 26, 2020, 10:24 am UTC
*/

class CarRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'position',
        'brand',
        'make',
        'model',
        'fuel_type',
        'engine',
        'mileage',
        'transmission',
        'gears_num',
        'body_style',
        'type',
        'state',
        'year',
        'price',
        'localisation',
        'status',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Car::class;
    }
}
