<?php

namespace App\Repositories\Admin;

use App\Models\Seller;
use App\Repositories\BaseRepository;

/**
 * Class SellerRepository
 * @package App\Repositories\Admin
 * @version May 28, 2020, 9:57 am UTC
*/

class SellerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'full_name',
        'phone_number',
        'email'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Seller::class;
    }
}
