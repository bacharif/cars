<?php

namespace App\Models;

use App\Models\Car;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $array)
 */
class CarImage extends Model
{
    public $table = 'car_images';

    public $fillable = [
        'name', 'car_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'car_id' => 'integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function car()
    {
        return $this->belongsTo(Car::class, 'car_id')->withTrashed();
    }

    /**
     * Get the path of the image
     * @return string
     */
    public function getImageUrlAttribute()
    {
        return asset('storage/cars/' . $this->car->id . '/' . $this->name);
    }
}
