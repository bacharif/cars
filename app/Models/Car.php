<?php

namespace App\Models;

use App\Models\CarImage;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Car
 * @package App\Models
 * @version April 26, 2020, 10:24 am UTC
 *
 * @property integer $id
 * @property string $reference
 * @property string $brand
 * @property string $make
 * @property string $model
 * @property string $fuel_type
 * @property string $engine
 * @property string $mileage
 * @property string $transmission
 * @property integer $gears_num
 * @property string $body_style
 * @property string $type
 * @property string $state
 * @property integer $year
 * @property integer $price
 * @property string $localisation
 * @property string $status
 * @property string $description
 * @property integer $seller_id
 * @property boolean $visibility
 * @method static all()
 * @method static whereVisibility(int $int)
 */
class Car extends Model
{
    use SoftDeletes;

    public $table = 'cars';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'reference',
        'position',
        'brand',
        'make',
        'model',
        'fuel_type',
        'engine',
        'mileage',
        'transmission',
        'gears_num',
        'body_style',
        'type',
        'state',
        'year',
        'price',
        'localisation',
        'status',
        'description',
        'seller_id',
        'visibility'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'reference' => 'string',
        'position' => 'integer',
        'brand' => 'string',
        'make' => 'string',
        'model' => 'string',
        'fuel_type' => 'string',
        'engine' => 'string',
        'mileage' => 'string',
        'transmission' => 'string',
        'gears_num' => 'integer',
        'body_style' => 'string',
        'type' => 'string',
        'state' => 'string',
        'year' => 'integer',
        'price' => 'integer',
        'localisation' => 'string',
        'status' => 'string',
        'description' => 'string',
        'seller_id' => 'integer',
        'visibility' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'brand' => 'required',
        'make' => 'required',
        'model' => 'required',
        'fuel_type' => 'required',
        'engine' => 'required',
        'mileage' => 'required',
        'type' => 'required',
        'state' => 'required',
        'year' => 'required',
        'price' => 'required',
        'localisation' => 'required',
        'status' => 'required',
        'description' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function carImages()
    {
        return $this->hasMany(CarImage::class, 'car_id');
    }

    public function seller()
    {
        return $this->belongsTo(Seller::class, 'seller_id');
    }
}
