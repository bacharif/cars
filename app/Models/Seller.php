<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Seller
 * @package App\Models
 * @version May 28, 2020, 9:57 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $cars
 * @property string $full_name
 * @property string $phone_number
 * @property string $email
 */
class Seller extends Model
{

    public $table = 'sellers';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'full_name',
        'phone_number',
        'email'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'full_name' => 'string',
        'phone_number' => 'string',
        'email' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'full_name' => 'required',
        'phone_number' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function cars()
    {
        return $this->hasMany(\App\Models\Car::class, 'seller_id');
    }
}
